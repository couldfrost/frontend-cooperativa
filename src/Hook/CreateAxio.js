
import axios from "axios"


const user = localStorage.getItem("UserSesion")
const userSesion = JSON.parse(user)
var token='';

if(!userSesion)
{
     token=null;
}
else{
     token=userSesion.token;
}

const apiUrl = 'https://cooperativallauquinquiri.com/api';
const authAxios = axios.create({
    baseURL: apiUrl,

    headers: {
        Authorization: `Bearer ${token}`
    }


});

export { authAxios,userSesion };

