import { useState } from "react";
import { authAxios } from "../CreateAxio";

import { validationForm } from "../../components/Validation/ValidationUser";
export const ChangerPassword = (inicialvariable, id,update) => {
    const [users, setUsers] = useState(inicialvariable);
    const [errors, setErrors] = useState([]);
    const [response, setResponse] = useState(null);
 
    const handleClose=()=>{
        window.location.reload();
    }
    const handlerChange = (e) => {
        setUsers(
            {
                ...users,
                [e.target.name]: e.target.value
            }
        )
    }
    const showUpdate=()=>{
        if(update==="ACCOUNT")
        {
            return false;
        }
        else{
            return true;
        }
    }
    const handlerBlur = (e) => {
        handlerChange(e);
        setErrors(validationForm(users, update));
    }

    const handlerSubmit = async e => {
        e.preventDefault();
        const newPassword = {
            password: users.password

        };
        setErrors(validationForm(users, update));
        if (Object.keys(errors).length === 0) {
            
            
            await authAxios.put(`/users/${id}`, newPassword);
                       
            alert("se cambio el password");
            window.location.reload();
        }
        else {
            return;

        }
        

    }
    
    return {
        users, errors, handleClose, response, handlerChange, handlerBlur, handlerSubmit,showUpdate
    }
}