import { useState } from "react";
import { authAxios,userSesion } from "../CreateAxio";
import { useNavigate } from 'react-router-dom';
import { validationForm } from "../../components/Validation/ValidationUser";

export const UserForm = (inicialvariable, update) => {
   const [users, setUsers] = useState(inicialvariable);
   const [errors, setErrors] = useState([]);
   const [response, setResponse] = useState(null);
   const navigate = useNavigate();
   
   const handleClose=()=>{
      navigate('/');
   }
   const handlerChange = (e) => {
      setUsers(
         {
            ...users,
            [e.target.name]: e.target.value
         }
      )
   }
   const showUpdate = () => {
      if (update === "UPDATE") {
         return true;
      }
      else {
         return false;
      }
   }
   const handlerBlur = (e) => {
      handlerChange(e);
      setErrors(validationForm(users, update));
   }
   const handlerSubmit = async e => {

      e.preventDefault();
      const newUser = {
         name: users.name,
         email: userSesion.userEmail,
         password: users.password         

      };
      
      setErrors(validationForm(users,update))
      if (Object.keys(errors).length === 0) {
 
         await authAxios.put('/users', newUser);
         
         navigate('/');
       
      }
      
   }
   return {
      users, errors, response, handlerChange,handleClose ,handlerBlur, handlerSubmit, showUpdate
   }
}