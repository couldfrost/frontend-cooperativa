import { useState, useEffect } from "react";
import { authAxios } from "../CreateAxio";
export const GetUserPrint = () => {

    const [userPrintP,setUserPrintP]=useState([]);
    const [userPrintT,setUserPrintT]=useState([]);
    useEffect(() => {
        const getData = async () => {
            const res = await authAxios.get('/users/'+2);
            setUserPrintP(res.data);
            const res2 = await authAxios.get('/users/'+3);
            setUserPrintT(res2.data);
        }
        getData();
    }, []);
    return {
        userPrintP,setUserPrintP,userPrintT,setUserPrintT
    }
}