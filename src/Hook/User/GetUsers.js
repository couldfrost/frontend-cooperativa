import { useState, useEffect } from "react";
import { authAxios } from "../CreateAxio";
export const GetUsers = () => {


    const [Users, setUsers] = useState([]);
    useEffect(() => {
        const getData = async () => {
            const res = await authAxios.get('/users');
            setUsers(res.data.reverse());
            
        }
        getData();
    }, []);
    return {
        Users,setUsers
    }
}