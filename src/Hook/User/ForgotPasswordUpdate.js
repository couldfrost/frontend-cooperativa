import { useState } from "react";
import { validationForm } from "../components/Validation/ValidationUser";
export const ForgotPasswordUpdate = (inicialvariable, update) => {
    const [users, setUsers] = useState(inicialvariable);
    const [errors, setErrors] = useState([]);
    const [loading, setLoading] = useState(false);
    const [response, setResponse] = useState(null);

    const showUpdate = () => {
        if (update === "ACCOUNT") {
            return false;
        }
        else {
            return true;
        }
    }
    const handlerChange = (e) => {
        setUsers(
            {
                ...users,
                [e.target.name]: e.target.value
            }
        )
    }
    const handlerBlur = (e) => {
        handlerChange(e);
        setErrors(validationForm(users, update));
    }
    const handlerSubmit = async e => {
        e.preventDefault();



    }

    return {
        users, errors, loading, response, handlerChange, handlerBlur, handlerSubmit, showUpdate
    }
}