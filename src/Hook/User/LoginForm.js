import { useState} from "react";
import { authAxios } from "../CreateAxio";
import { useNavigate } from 'react-router-dom';
import { Sesion } from "./Sesion";

export const LoginForm = (inicialvariable) => {
    const [logins, setLogins] = useState(inicialvariable);
    const [errors, setErrors] = useState([]);
    const {setUserSesion} = Sesion();
    const [response, setResponse] = useState(null);
    
    const navigate = useNavigate();
    const handlerChange = (e) => {
        setLogins(
            {
                ...logins,
                [e.target.name]: e.target.value
            }
        )
    }
    const handlerBlur = (e) => {
        handlerChange(e);
        setErrors();
    }
    
    const handlerSubmit = async e => {
        let errorSesion={}
        e.preventDefault();
        const newSesion = {
            email: logins.email,
            password: logins.password
        };
        const user = await authAxios.post('/login', newSesion);
        
        if (user.data === "") {
            setLogins({
                email: '',
                password: ''
            });
            errorSesion.user="el email o password incorrecto";
            setErrors(errorSesion);
        }
        else {
            navigate('/');
            setUserSesion(JSON.stringify(user.data));
            window.location.reload();
            
        }
        
        

    }
    return {
        logins, errors, setResponse, response, handlerChange, handlerBlur, handlerSubmit,
    }
}