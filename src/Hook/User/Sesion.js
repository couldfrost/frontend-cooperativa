import { useContext, useEffect } from "react";
import { UserContext } from '../../components/UserContext';
export const Sesion = () => {


    const {removeSesion, userSesion, setUserSesion } = useContext(UserContext);
   
    useEffect(() => {
        setUserSesion(localStorage.getItem("UserSesion"))
    }, []);
    return {
        removeSesion, userSesion,setUserSesion
    }
}