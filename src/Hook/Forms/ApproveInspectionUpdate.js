import { useState } from "react";
import { authAxios } from "../CreateAxio";
import { useNavigate } from 'react-router-dom';
import { validationForm } from "../../components/Validation/ValidationApproveInspeccion";
export const ApproveInspectionUpdte = (inicialvariable, proopInspection) => {
    const [approves, setApproves] = useState(inicialvariable);
    const [errors, setErrors] = useState([]);
    const [response, setResponse] = useState("Ya se reviso el informe del reclamo");
    const navigate = useNavigate();
    const handlerChange = (e) => {
        setApproves(
            {
                ...approves,
                [e.target.name]: e.target.value
            }
        )
    }
    const handlerBlur = (e) => {
        handlerChange(e);
        setErrors(validationForm(approves));
    }
    const handlerSubmit = async e => {
        e.preventDefault();
        const updateClaim = {
            states: approves.states,
            comment: approves.comment
          };
          await authAxios.put('/claims/' + proopInspection.claims_id, updateClaim);
          navigate('/Listclaims',{state:{response}  
        });


    }
    const handlerSubmitWorkDone = () => {
        navigate('/CreateWorkDone', {
            state: { proopInspection }
        });
    }
    return {
        approves, errors, response, handlerChange, handlerBlur, handlerSubmit, handlerSubmitWorkDone
    }
}