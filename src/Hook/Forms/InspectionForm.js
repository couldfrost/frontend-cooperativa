import { useState } from "react";
import { authAxios } from "../CreateAxio";
import { useNavigate } from 'react-router-dom';
import { validationForm } from "../../components/Validation/ValidationInspeccion";
export const InspectionForm = (inicialvariable) => {
    const [inspections, setInspections] = useState(inicialvariable);
    const [errors, setErrors] = useState([]);
    const [response, setResponse] = useState("Se creo exitosamente el informe de inspección");
    const navigate = useNavigate();
    const handlerChange = (e) => {
        setInspections(
            {
                ...inspections,
                [e.target.name]: e.target.value
            }
        )
    }
    const handlerBlur = (e) => {
        handlerChange(e);
        setErrors(validationForm(inspections));
    }
    const handlerSubmit = async e => {

        e.preventDefault();
        
        const newInspection = {
                claims_id: inspections.claims_id,
                name_plumber: inspections.name_plumber,
                description: inspections.description,
                inspections_date: inspections.inspections_date,
                name_pay: inspections.name_pay

            };
        setErrors(validationForm(inspections))
        if (Object.keys(errors).length === 0) {
            

            await authAxios.post('/inspections', newInspection);
            
            navigate('/ListInspections',{state:{response}});
        }
        else {
            return;
        }
    }
    return {
        inspections, errors, response,setResponse, handlerChange, handlerBlur, handlerSubmit
    }
}