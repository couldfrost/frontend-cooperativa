import { useState } from "react";
import { authAxios } from "../CreateAxio";
import { useNavigate } from 'react-router-dom';
import { validationForm } from "../../components/Validation/ValidationPartner";
export const PartnerForm = (inicialvariable) => {
    const [partners, setPartners] = useState(inicialvariable);
    const [errors, setErrors] = useState([]);
    const [response, setResponse] = useState("Se creo exitosamente el socio");

    const navigate = useNavigate();
    const handlerChange = (e) => {
        setPartners(
            {
                ...partners,
                [e.target.name]: e.target.value

            }
        )

    }
    const handlerBlur = (e) => {
        handlerChange(e);
        setErrors(validationForm(partners));
    }
    const handlerSubmit = async e => {

        e.preventDefault();
        
        const newPartner = {
            ci: partners.ci,
            partner_name: partners.partner_name,
            surname_father: partners.surname_father,
            surname_mother: partners.surname_mother,
            date_birth: partners.date_birth,
            direction: partners.direction,
            phone: partners.phone,
            stated: partners.stated


        };
        setErrors(validationForm(partners));
        if (Object.keys(errors).length === 0) {
            
            const errorPartner = {};
            const res = await authAxios.post('/partners', newPartner);
            
            if (res.data === "") {
                errorPartner.partner = "Socio ya existe"
                setErrors(errorPartner);


            }
            else {
                
                navigate('/ListPartners', { state: { response } });

            }

        }
        else {

            return;
        }

    }
    return {
        partners, errors, setResponse, handlerChange, handlerBlur, handlerSubmit,
    }
}