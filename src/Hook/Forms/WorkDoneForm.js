import { useState } from "react";
import { authAxios } from "../CreateAxio";
import { useNavigate } from 'react-router-dom';
import { validationForm } from "../../components/Validation/ValidationWorkDone";
export const WorkDoneForm = (inicialvariable) => {
    const [workDones, setWorkDones] = useState(inicialvariable);
    const [errors, setErrors] = useState([]);
    const navigate = useNavigate();
    const handlerChange = (e) => {
        setWorkDones(
            {
                ...workDones,
                [e.target.name]: e.target.value
            }
        )
    }
    const handlerBlur = (e) => {
        handlerChange(e);
        setErrors(validationForm(workDones,"REGISTER"));
    }
    const handlerSubmit = async e => {
        const Iid = workDones.inspections_id;

        e.preventDefault();
        const newWorkDone = {
            inspections_id: workDones.inspections_id,
            claims_id: workDones.claims_id,
            date: workDones.date,
            description: workDones.description,
            repair_cost: workDones.repair_cost,
            stated: workDones.stated

        };

        setErrors(validationForm(workDones,"REGISTER"))
        if (Object.keys(errors).length === 0) {
            
            await authAxios.post('/workDone', newWorkDone);
            navigate('/CreateListMaterial', {
                state: { Iid }
            });
        }
        else {
            return;
        }
    }
    return {
        workDones, errors, handlerChange, handlerBlur, handlerSubmit,
    }
}