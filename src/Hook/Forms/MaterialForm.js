import { useState } from "react";
import { authAxios } from "../CreateAxio";
import { validationForm } from "../../components/Validation/ValidationMaterial";
import {ShowModal} from '../MapAndShow/ShowModal'
export const MaterialForm = (inicialvariable) => {
   const [materials, setMaterials] = useState(inicialvariable);
   const {show,setShow,handleClose,handleShow}=ShowModal();
   const [errors, setErrors] = useState([]);
   
   
   const handlerChange = (e) => {
      setMaterials(
         {
            ...materials,
            [e.target.name]: e.target.value
         }
      )
   }
   const handlerBlur = (e) => {
      handlerChange(e);
      setErrors(validationForm(materials));
   }
   const handlerSubmit = async e => {
      e.preventDefault();
      const newMaterial = {
         material_name: materials.material_name
      }
      const errorMaterial = {};
      setErrors(validationForm(materials));
      if (Object.keys(errors).length === 0) {
         const res = await authAxios.post('/material', newMaterial);
         if (res.data === "") {
            errorMaterial.material = "este articulo ya existe";
            setErrors(errorMaterial);
         }
         else {
            setShow(false);
            window.location.reload();
         }

      }

      else {
         return;
      }

   }
   
   return {
      materials, show, errors, handlerChange, handlerBlur, handlerSubmit, handleClose, handleShow
   }
}