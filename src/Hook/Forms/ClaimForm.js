import { useState } from "react";
import { authAxios } from "../CreateAxio";
import { useNavigate } from 'react-router-dom';
import { validationForm } from "../../components/Validation/ValidationClaim";
import { ShowModal } from "../MapAndShow/ShowModal";
import { MapPosition } from "../MapAndShow/MapPosition";
export const ClaimForm = (inicialvariable, mapvariable, user) => {
   const [claims, setClaims] = useState(inicialvariable);
   const [errors, setErrors] = useState([]);
   const [partnerId, setPartnerId] = useState(inicialvariable);
   const [response, setResponse] = useState("Se creo el reclamo exitosamente");
   const { show, setShow, handleClose, handleShow } = ShowModal();
   const [posMap, setPosMap] = useState();
   const { positions } = MapPosition(mapvariable);
   const navigate = useNavigate();

   const handlerChange = (e) => {
      setClaims(
         {
            ...claims,
            [e.target.name]: e.target.value
         }
      )
   }
   const handlerBlur = (e) => {
      handlerChange(e);
      setErrors(validationForm(claims));
   }

   const handlerSubmit = async e => {

      e.preventDefault();
      var newClaim = {};
      if (posMap) {
         newClaim = {
            partner_id: claims.partner_id,
            claims_date: claims.claims_date,
            phone: claims.phone,
            meter_number: claims.meter_number,
            direction: claims.direction,
            descriptions: claims.descriptions,
            states: claims.states,
            lat: posMap.lat,
            lng: posMap.lng

         };
      }
      else {
         newClaim = {
            partner_id: claims.partner_id,
            claims_date: claims.claims_date,
            phone: claims.phone,
            meter_number: claims.meter_number,
            direction: claims.direction,
            descriptions: claims.descriptions,
            states: claims.states,
            lat: 0,
            lng: 0

         };
      }

      setErrors(validationForm(claims));
      if (Object.keys(errors).length === 0) {

         await authAxios.post('/claims', newClaim)
         if (!user) {
            navigate('/ClaimsPartner', { state: { partnerId, response } });
         }
         else {
            navigate('/ListClaims', { state: { response } })
         }
      }
      else {
         return;
      }


   }

   const handlerMaps = () => {
      setPosMap(positions)
      setShow(false);
   }
   return {
      positions, claims, errors, show, setResponse, handlerMaps, handleClose, handleShow, handlerChange, handlerBlur, handlerSubmit,
   }
}