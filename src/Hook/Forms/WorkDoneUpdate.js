import { useState } from "react";
import { authAxios } from "../CreateAxio";
import { ShowModal } from "../MapAndShow/ShowModal";
import { useNavigate } from "react-router-dom";
import { validationForm } from "../../components/Validation/ValidationWorkDone";
export const WorkDoneUpdte = (inicialvariable, proopWorkDone, respon) => {
    const { show, setShow, handleClose, handleShow } = ShowModal();
    const [approves, setApproves] = useState(inicialvariable);
    const [errors, setErrors] = useState([]);
    const navigate = useNavigate();
    const handlerChange = (e) => {
        setApproves(
            {
                ...approves,
                [e.target.name]: e.target.value
            }
        )
    }
    const handlerBlur = (e) => {
        handlerChange(e);
        setErrors(validationForm(approves, "UPDATE"));
    }
    const handlerSubmit = async e => {
        e.preventDefault();
        const updateWorkDone = {
            stated: approves.stated,
            repair_cost: approves.repair_cost
        };
        setErrors(validationForm(approves, "UPDATE"))
        if (Object.keys(errors).length === 0) {
            await authAxios.put('/workDone/' + proopWorkDone, updateWorkDone);
            setShow(false);
            window.location.reload()
        }
        else { return }


    }
    const handlerSubmitMaterials = () => {
        const Iid = respon.inspections_id;

        navigate('/CreateListMaterial', {
            state: { Iid }
        });

    }

    return {
        approves, show, errors, handlerChange, handlerBlur, handlerSubmit, handleShow, handleClose, handlerSubmitMaterials
    }
}