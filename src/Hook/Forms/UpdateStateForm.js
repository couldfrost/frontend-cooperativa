import { useState } from "react";
import { authAxios } from "../CreateAxio";
import { ShowModal } from '../MapAndShow/ShowModal'
export const UpdateStateForm = (inicialvariable, id) => {
    const [update, setUpdate] = useState(inicialvariable);
    const { show, setShow, handleClose, handleShow } = ShowModal();
    
    const handlerChange = (e) => {
        setUpdate(
            {
                ...update,
                [e.target.name]: e.target.value
            }
        )
    }
   
    const handlerSubmit = async e => {
        e.preventDefault();
        const newUpdate = {
            stated: update.stated
        }
        
        await authAxios.put('/partners/' + id, newUpdate);
        setShow(false);
        window.location.reload()

    }

    return {
        update, show, handlerChange, handlerSubmit, handleClose, handleShow
    }
}