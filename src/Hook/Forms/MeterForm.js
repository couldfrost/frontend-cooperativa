import { useState } from "react";
import { authAxios } from "../CreateAxio";
import { useNavigate } from 'react-router-dom';
import { validationForm } from "../../components/Validation/ValidationMeter";

export const MeterForm = (inicialvariable) => {
    const [meters, setMeters] = useState(inicialvariable);
    const [errors, setErrors] = useState([]);    
    const [response, setResponse] = useState(null);
    const navigate = useNavigate();
    const handlerChange = (e) => {
        setMeters(
            {
                ...meters,
                [e.target.name]: e.target.value
            }
        )
    }
    const handlerBlur = (e) => {
        handlerChange(e);
        setErrors(validationForm(meters));
    }
    const handlerSubmit = async e => {
        e.preventDefault();
        const newMeter = {
            number_meter: meters.number_meter,
            direction_meter: meters.direction_meter,
            date_registered: meters.date_registered,
            partner_id: meters.partner_id

        };
        setErrors(validationForm(meters));
        if (Object.keys(errors).length === 0) {
            const errorMeter = {};


            const res = await authAxios.post('/Meters', newMeter);


            if (res.data === "") {
                errorMeter.meter = "El socio ya tiene este medidor"
                setErrors(errorMeter);


            }
            else {

                navigate('/ListPartners');

            }
        }
        else {
            return;
        }


    }
    return {
        meters, errors,setResponse, response, handlerChange, handlerBlur, handlerSubmit,
    }
}