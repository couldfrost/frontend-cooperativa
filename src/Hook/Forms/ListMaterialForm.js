import { useState } from "react";
import { authAxios } from "../CreateAxio";
import { useNavigate } from 'react-router-dom';
import { validationForm } from "../../components/Validation/ValidationListMaterial";
export const ListMaterialForm = (inicialvariable,id) => {
    const [addMaterials, setAddMaterials] = useState(inicialvariable);
    const [errors, setErrors] = useState([]);
    const [response, setResponse] = useState("Se creo exitosamente el informe de trabajo realizado");
        
    const idWorkDone=id;
    const navigate = useNavigate();
    const handlerChange = (e) => {
        setAddMaterials(
            {
                ...addMaterials,
                [e.target.name]: e.target.value

            }
        )
        
    }
    const handlerBlur = (e) => {
        handlerChange(e);
        setErrors(validationForm(addMaterials));
    }
    const handlerSubmit = async e => {
        
        e.preventDefault();
        const newMaterial = {
            work_done_id: idWorkDone,
            article: addMaterials.article,
            amount: addMaterials.amount,
            observation: addMaterials.observation

        };
        setErrors(validationForm(addMaterials))
        if(Object.keys(errors).length===0)
        {
        await authAxios.post('/listMaterial', newMaterial);        
        
        setAddMaterials({
            article: ' ',
            amount: ' ',
            observation: ' '
        });
        window.location.reload();
        }
        else{
            return;
        }
    }
    const onSubmit=()=>{
        navigate('/ListWorkDone',{state:{response}})
    }
    return {
        addMaterials, errors, response,onSubmit,setResponse ,handlerChange, handlerBlur, handlerSubmit,
    }
}