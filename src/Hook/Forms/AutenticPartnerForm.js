import { useState } from "react";
import { authAxios } from "../CreateAxio";
import { useNavigate } from 'react-router-dom';
export const AuthenticPartnerForm = (inicialvariable, type) => {
   const [authentic, setAuthentic] = useState(inicialvariable);
   const [errors, setErrors] = useState([]);

   const [response, setResponse] = useState(null);
   const navigate = useNavigate();
   const handlerChange = (e) => {
      setAuthentic(
         {
            ...authentic,
            [e.target.name]: e.target.value
         }
      )
   }

   const handlerSubmit = async e => {
      const errorAuthentic = {}
      e.preventDefault();

      const authenticCi = {
         ci: authentic.ci
      };
      const id = await authAxios.post('/partners/ci', authenticCi);
      const partnerId = id.data[0];


      if (!partnerId) {

         errorAuthentic.authentic = "El socio no fue encontrado"
         setErrors(errorAuthentic);
         setAuthentic({
            ci: ' '
         });
      }
      else {
         if (partnerId.stated === "ACTIVO") {
            if (type === "CREATE") {
               navigate('/CreateClaim', {
                  state: { partnerId }
               });
            }
            if (type === "SHOW") {
               navigate('/ClaimsPartner', {
                  state: { partnerId }
               });
            }
         }
         else {
            errorAuthentic.authentic = "El socio no esta ACTIVO"
            setErrors(errorAuthentic);
            setAuthentic({
               ci: ' '
            });
         }

      }





   }
   return {
      authentic, errors, response, handlerChange, handlerSubmit,
   }
}