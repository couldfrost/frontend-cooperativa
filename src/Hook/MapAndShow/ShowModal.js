import { useState } from "react";


export const ShowModal = ( ) => {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return {
        show,setShow,handleClose,handleShow
    }
}