import { useState, useLayoutEffect } from "react";

export const MapPosition = (mapvariable) => {
   
  
   const [positions, setPositions] = useState(mapvariable);
   

   useLayoutEffect(() => {
      if (!!navigator.geolocation) {
        

         navigator.permissions.query({ name: "geolocation" }).then(function (result) {
           
            if (result.state === "granted") {
             
               navigator.geolocation.watchPosition(success);
            } else if (result.state === "prompt") {
               navigator.geolocation.watchPosition(success, error, options);
            } else if (result.state === "denied") {
               alert('El navegador necesita permiso para acceder a su ubicacion')
               //If denied then you have to show instructions to enable location
            }
            result.onchange = function () {
               console.log(result.state);
            };
         })

      } else {
         //  // No Support Web
         alert('El navegador no soporta la geolocalización,')
      }
   }, []);
   
   const success = (positions) => {
      setPositions({
         lat: positions.coords.latitude,
         lng: positions.coords.longitude,
      });
   }

   const error = (err) => {
      console.log(err)
   }
   const options = {
      enableHighAccuracy: true, timeout: 10000, maximumAge: 10000
   }
   
   return {
      positions
   }
}