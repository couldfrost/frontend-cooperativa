import { useState } from "react";


export const ShowMark = () => {
  const [selectedElement, setSelectedElement] = useState(null);
  const [activeMarker, setActiveMarker] = useState(null);
  const [showInfoWindow, setInfoWindowFlag] = useState(true);
  return {
    selectedElement, setActiveMarker, activeMarker, setInfoWindowFlag, showInfoWindow, setSelectedElement
  }
}