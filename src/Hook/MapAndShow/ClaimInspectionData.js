import { useState } from "react";
import { useNavigate } from 'react-router-dom';
export const ClaimInspectionData = (inicialvariable) => {
   const claim = inicialvariable;
   const navigate = useNavigate();
   const onSubmit = () => {
      navigate('/CreateInspection', { state: { claim } });
   }
   const [position, setPosition] = useState({
      lat: inicialvariable.lat,
      lng: inicialvariable.lng
   })
   return {
      position, onSubmit
   }
}