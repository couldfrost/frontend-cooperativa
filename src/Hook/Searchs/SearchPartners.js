import { useState } from "react";
import { authAxios } from "../CreateAxio";
import { GetPartners } from "../Gets/GetPartners";
export const SearchPartners = () => {
    const [ci, setCi] = useState('');
    const {partners,setPartners}=GetPartners()

    const onSubmit = async e => {
        e.preventDefault();
        const seachCi = {
            ci
        };
                
        const res = await authAxios.post('/partners/ci', seachCi);
               
        if (res.data.length === 0) {
            setCi(' ');
            setPartners(res.data);
        }
        else
        {
            setPartners(res.data);
            setCi(' ');
        }
    }

    const onCiChange = (e) => {
        setCi(e.target.value);
    }

    return {
        ci,partners,onCiChange,onSubmit
    }
}