import { useState } from "react";
import { authAxios } from "../CreateAxio";
import { GetClaims } from "../Gets/GetClaims";
export const SearchHistory = (inicialvariable) => {

    const {claims,setClaims}=GetClaims();
    const [errors, setErrors] = useState([]);
    const [search, setSearch] = useState(inicialvariable);
    
      
    const handlerChange = (e) => {
        setSearch(
            {
                ...search,
                [e.target.name]: e.target.value

            }
        )

    }


    const onSubmit = async e => {
        e.preventDefault();
        const searchHistory = {
            month: JSON.parse(search.month),
            year: JSON.parse(search.year)
        };
                
        const errorHistory={}
        const res = await authAxios.post('/claims/history', searchHistory);
           
        if (res.data.length === 0) {
            setSearch({
                month:' ',
                year:' '
            })
            setClaims(res.data);
            errorHistory.history="NO HAY NADA EN ESTA FECHA"
            setErrors(errorHistory);
        }
        else {
            setClaims(res.data.reverse());
            setSearch({
                month:' ',
                year:' '
            })
            setErrors(errorHistory);
        }



    }
    
    return {
        claims, errors,search,setSearch, handlerChange, onSubmit
    }
}