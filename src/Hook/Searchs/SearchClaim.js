import { useState } from "react";
import { GetClaims } from "../Gets/GetClaims";
import { authAxios } from "../CreateAxio";
export const SearchClaim = ( ) => {
        
    const [errors, setErrors] = useState([]);    
    const [search, setSearch] = useState('');
    const [selection, setSelection] = useState('');
    const { claims, setClaims,currentClaim,claimsPerPage,paginate,setCurrentPage } = GetClaims();
    
    
    const onSelectionChange = (e) => {
        setSelection(e.target.value);
    }
    const onSearchChange = (e) => {
        setSearch(e.target.value);
    }


    const onSubmit = async e => {
        e.preventDefault();
        const searchNumber = {
            search
        };
        const errorClaim = {};
        var res={};
        switch (selection) {
            case "MEDIDOR":
                 res = await authAxios.post('/claims/meter', searchNumber);
                break;
            case "CI":
                 res = await authAxios.post('/claims/ci', searchNumber);
                break;

            default:
                errorClaim.claims = "NO TIENE RECLAMOS"
                setErrors(errorClaim);


        }
        if (res.data.length === 0) {
            setSearch(' ');
            setClaims(res.data.reverse());
            errorClaim.claims = "NO TIENE RECLAMOS"
            setErrors(errorClaim);
        }
        else {
            
            setClaims(res.data.reverse());
            setSearch(' ');
            setErrors(errorClaim);
        }



    }

    return {
        paginate,claimsPerPage,currentClaim, claims, errors, selection, search, onSearchChange, onSelectionChange, onSubmit
    }
}