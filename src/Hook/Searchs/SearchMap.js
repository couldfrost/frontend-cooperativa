import { useState } from "react";
import { authAxios } from "../CreateAxio";
export const SearchMap = (inicialvariable) => {

    const [errors, setErrors] = useState([]);
    const [search, setSearch] = useState(inicialvariable);
    const [ubications, setUbications] = useState([]);
    const handlerChange = (e) => {
        setSearch(
            {
                ...search,
                [e.target.name]: e.target.value

            }
        )

    }


    const onSubmit = async e => {
        e.preventDefault();
        
        const searchHistory = {
            month: JSON.parse(search.month),
            year: JSON.parse(search.year)
        };
        let res={}
        const errorUbication={}
        res = await authAxios.post('/claims/maps', searchHistory);
          
        if (res.data.length === 0) {
            setSearch({
                month:' ',
                year:' '
            })
            setUbications(res.data);
            errorUbication.Ubications="NO EXISTE UBICACIONES EN ESTA FECHA"
            setErrors(errorUbication);
        }
        else {
            setUbications(res.data);
            setSearch({
                month:' ',
                year:' '
            })
            setErrors(errorUbication);
        }



    }
    return {
        ubications, errors,search,setSearch, setUbications, handlerChange, onSubmit
    }
}