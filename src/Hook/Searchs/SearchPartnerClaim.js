import { useState,useEffect } from "react";
import { authAxios } from "../CreateAxio";
import { GetClaims } from "../Gets/GetClaims";
export const SearchPartnerClaim = (inicialvariable) => {

    const [errors, setErrors] = useState([]);
    const [search, setSearch] = useState(inicialvariable);
    const { claims, setClaims, currentClaim, claimsPerPage, paginate, setCurrentPage } = GetClaims();
   
    useEffect(() => {
        const getData = async () => {
            const searchNumber = {
                search
            };
            const errorClaim = {};
    
    
            const res = await authAxios.post('/claims/ci', searchNumber);
            setClaims(res.data.reverse())
            if (res.data.length === 0) {
                setSearch(' ');
                setClaims(res.data);
                errorClaim.claims = "NO TIENE RECLAMOS"
                setErrors(errorClaim);
            }
            else {
    
                setClaims(res.data.reverse());
                setSearch(' ');
                setErrors(errorClaim);
            }
        }
        getData();
       
    }, []);
    
  

    return {
        paginate, claimsPerPage, currentClaim, claims, errors, search
    }
}