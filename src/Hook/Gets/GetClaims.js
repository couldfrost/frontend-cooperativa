import { useState,useEffect } from "react";
import { authAxios } from "../CreateAxio";
export const GetClaims = () => {
    
    const [claims, setClaims] = useState([]);
    const [currentPage,setCurrentPage]=useState(1);
    const [claimsPerPage,setClaimsPerPage]=useState(5);
    
    
    useEffect(() => {
        const getData = async () => {
            const res = await authAxios.get(`/claims`);
            setClaims(res.data.reverse());
            
        }
        getData();
    }, []);
    
    const indexOfLastClaim=currentPage*claimsPerPage;
    const indexOfFirstClaim=indexOfLastClaim-claimsPerPage;
    const currentClaim=claims.slice(indexOfFirstClaim,indexOfLastClaim);
    
    const paginate=(pageNumber)=>setCurrentPage(pageNumber);

    return {
        claims,setClaims,currentClaim,claimsPerPage,paginate,setCurrentPage
    }
}