import { useState, useEffect } from "react";
import { authAxios } from "../CreateAxio";
export const GetOneWorkDone = (variable) => {

  const [work, setWork] = useState([]);


  useEffect(() => {

    const getData = async () => {


      const res = await authAxios.get(`/WorkDone/${variable}`);

      setWork(res.data);

    }
    getData();
  }, []);
  return {
    work, setWork
  }
}