import { useState, useEffect } from "react";
import { authAxios } from "../CreateAxio";
export const GetMaterials = () => {

    const [materials, setMaterials] = useState([]);
   
    useEffect(() => {
        const getData = async () => {
            const resMaterial = await authAxios.get('/material');

            setMaterials(resMaterial.data.reverse());

        }
        getData();

    }, []);
    return {
        materials,setMaterials
    }
}