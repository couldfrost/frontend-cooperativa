import { useState, useEffect } from "react";
import { authAxios } from "../CreateAxio";
export const GetInspections = () => {


  const [inspections, setInspections] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [inspectionsPerPage, setInspectionsPerPage] = useState(5);
  useEffect(() => {

    const getData = async () => {

      const res = await authAxios.get('/inspections');

      setInspections(res.data.reverse());

    }
    getData();
  }, []);

  const indexOfLastInspections = currentPage * inspectionsPerPage;
  const indexOfFirstInspections = indexOfLastInspections - inspectionsPerPage;
  const currentInspections = inspections.slice(indexOfFirstInspections, indexOfLastInspections);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);
  return {
    inspections, setInspections,currentInspections,paginate,setCurrentPage,inspectionsPerPage
  }
}