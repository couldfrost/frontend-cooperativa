import { useState, useEffect } from "react";
import { authAxios } from "../CreateAxio";
export const GetClaimsPartner = (inicialvariable) => {

    const [claims, setClaims] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [claimsPerPage, setClaimsPerPage] = useState(5);
    const [search, setSearch] = useState(inicialvariable);
    const [errors, setErrors] = useState([]);

    useEffect(() => {
        const getData = async () => {
            const searchNumber = {
                search
            };
            const errorClaim = {};


            const res = await authAxios.post('/claims/ci', searchNumber);
            
            if (res.data.length === 0) {
                setSearch(' ');
                setClaims(res.data);
                errorClaim.claims = "NO TIENE RECLAMOS"
                setErrors(errorClaim);
            }
            else {

                setClaims(res.data.reverse());
                setSearch(' ');
                setErrors(errorClaim);
            }
        }
        getData();
    }, []);
        
    const indexOfLastClaim = currentPage * claimsPerPage;
    const indexOfFirstClaim = indexOfLastClaim - claimsPerPage;
    const currentClaim = claims.slice(indexOfFirstClaim, indexOfLastClaim);

    const paginate = (pageNumber) => setCurrentPage(pageNumber);

    return {
        claims,errors, setClaims, currentClaim, claimsPerPage, paginate, setCurrentPage
    }
}
