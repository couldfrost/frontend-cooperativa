import { useState, useEffect } from "react";
import { authAxios } from "../CreateAxio";
export const GetPartners = () => {


    const [partners, setPartners] = useState([]);

    useEffect(() => {

        const getData = async () => {
            const res = await authAxios.get('/partners');

            setPartners(res.data.reverse());
            

        }
        getData();
    }, []);
    return {
        partners,setPartners
    }
}