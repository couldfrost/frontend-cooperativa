import { useState, useEffect } from "react";
import { authAxios } from "../CreateAxio";
export const GetWorkDone = () => {

    const [workDones, setWorkDones] = useState([]);
    useEffect(() => {
        const getData = async () => {
            const res = await authAxios.get('/workDone');
            setWorkDones(res.data.reverse());
            
        }
        getData();
    }, []);
    return {
        workDones,setWorkDones
    }
}