import { useState, useEffect } from "react";
import { authAxios } from "../CreateAxio";

export const GetOneWorkDoneForIdIspection = (variable) => {

    const [workDone, setworkDone] = useState();
    const [listMaterials, setListMaterials] = useState([]);


    useEffect(() => {
        const getData = async () => {
            const resWorkDone = await authAxios.get('/workDone/idInspection/' + variable);
            setworkDone(resWorkDone.data.work_done_id);

            setListMaterials(resWorkDone.data.lista_materiales.reverse())
        }
        getData();

    }, []);
    return {
        workDone,listMaterials,setListMaterials,setworkDone
    }
}