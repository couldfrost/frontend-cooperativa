import React from 'react'
import { Pagination } from 'react-bootstrap';
function Paginacion({ claimsPerPage, totalClaims, paginate }) {
    const pageNumbers = [];

    for (let i = 1; i <= Math.ceil(totalClaims / claimsPerPage); i++) {
        pageNumbers.push(i);
    }

    return (
        <div className='pag'>
            <Pagination>
                {
                    pageNumbers.map(number => (
                        <Pagination.Item key={number} onClick={() => paginate(number)}>
                            {number}
                        </Pagination.Item>
                    ))
                }
            </Pagination>

        </div>
    )
}

export default Paginacion
