import React from "react";
import {  Navigate } from "react-router-dom";

function PrivateRoute({ children }){
    const user=localStorage.getItem("UserSesion")
    const userSesion=JSON.parse(user)    
    
    return (userSesion ? children:<Navigate to="/Login" />);

}
export default PrivateRoute