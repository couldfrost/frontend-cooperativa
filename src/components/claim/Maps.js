import React, { useMemo } from 'react'
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"
import credentials from '../../function/credentials';
function Maps({ resPosition }) {


    const MyMapComponent = withScriptjs(withGoogleMap((props) =>
        <GoogleMap
            defaultZoom={16}
            defaultCenter={{ lat: resPosition.lat, lng: resPosition.lng }}
        >
            {props.isMarkerShown ? <> <Marker position={{ lat: resPosition.lat, lng: resPosition.lng }} />
            </> : <></>}
        </GoogleMap>
    ))
    
    const map = useMemo(() => <MyMapComponent
        isMarkerShown
        googleMapURL= {credentials.URL}
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `400px` }} />}
        mapElement={<div style={{ height: `100%` }} />}
    />);
    return (
        <div>
            {map}
        </div>
    )
}

export default Maps
