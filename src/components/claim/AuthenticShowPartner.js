import React from 'react'
import { AuthenticPartnerForm } from '../../Hook/Forms/AutenticPartnerForm'
import SearchPartner from './SearchPartner';
function AuthenticShowPartner() {
    const authenticPart = {
        ci: ''
    };
    return (
        <SearchPartner res={AuthenticPartnerForm(authenticPart,"SHOW")}/>
        
    )
}

export default AuthenticShowPartner
