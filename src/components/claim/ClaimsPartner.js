import React from 'react'
import { useLocation } from 'react-router-dom';
import { GetClaimsPartner } from '../../Hook/Gets/GetClaimsPartner';
import ShowClaims from './ShowClaims';
import { Sesion } from '../../Hook/User/Sesion';

function ClaimsPartner() {
    const location = useLocation();
    const ci=location.state.partnerId.ci;
    
    const { userSesion } = Sesion();
  return (
    <ShowClaims res={GetClaimsPartner(ci)} menssage={location} sesion={userSesion}/>
  )
}

export default ClaimsPartner
