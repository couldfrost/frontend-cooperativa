import React from 'react'
import { Modal,Button } from 'react-bootstrap'
import Maps from './Maps'
function ShowMaps({hookClaim}) {
    
  return (
    <>
    <Button onClick={hookClaim.handleShow}>Ver</Button>

    <Modal show={hookClaim.show} onHide={hookClaim.handleClose}>
        <Modal.Header closeButton>
            <Modal.Title >VERIFICACIÓN UBICACIÓN</Modal.Title>
        </Modal.Header>
        <Modal.Body>


            <Maps resPosition={hookClaim.positions}/>


        </Modal.Body>
        {
        hookClaim.showClaim()?    
        <Modal.Footer>

            <Button onClick={hookClaim.handlerMaps}>Confirmar</Button>

        </Modal.Footer>:<></>
        }
    </Modal>
    </>
  )
}

export default ShowMaps
