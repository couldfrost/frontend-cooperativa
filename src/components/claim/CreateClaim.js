import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../../Styles/styleClaim.css'
import { ClaimForm } from '../../Hook/Forms/ClaimForm'
import { Modal, Alert, Container, Button, Col, Card, FormGroup, Form, InputGroup, FormControl, Row } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';
import { Sesion } from '../../Hook/User/Sesion';
import Maps from './Maps';

function CreateClaim() {
    const location = useLocation();
    const { userSesion } = Sesion();
    const Pid = location.state.partnerId.partner_id;
    const partner = location.state.partnerId.partner_name;
    const ApF = location.state.partnerId.surname_father;
    const ApM = location.state.partnerId.surname_mother;
    const meters = location.state.partnerId.medidores;
    const ci=location.state.partnerId.ci;

    const claim = {
        partner_id: Pid,
        claims_date: new Date(),
        phone: '',
        meter_number: '',
        direction: '',
        descriptions: '',
        states: "PENDIENTE",
        lat: 0,
        lng: 0,
        ci: ci
    };
    const position = {
        lat: 0,
        lng: 0
    }
    const {
        positions, claims, errors, show, handlerMaps, handleClose, handleShow, handlerChange, handlerBlur, handlerSubmit,
    } = ClaimForm(claim, position, userSesion)
  
   
    return (
        <Container className='ContBodyCret'>
            <Row>
                <Col md={{ span: 10, offset: 1 }}>
                    <Card.Body>
                        <h3 className='title'>CREAR NUEVO RECLAMO</h3>
                        <FormGroup className='bodyP'>


                            <InputGroup className="mb-3" >
                                <InputGroup.Text id="inputGroup-sizing-default" className='dateC'>FECHA</InputGroup.Text>
                                <FormControl
                                    placeholder={new Date().toLocaleDateString()}
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    disabled
                                />
                            </InputGroup>
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='borderLeft-FS'>SOCIO</InputGroup.Text>
                                <FormControl
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    placeholder={partner + " " + ApF + " " + ApM}
                                    disabled
                                />
                            </InputGroup>
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='borderLeft-C'>N° CONTACTO</InputGroup.Text>
                                <FormControl
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='phone'
                                    value={claims.phone}

                                    onChange={handlerChange}
                                />
                            </InputGroup>
                            {errors.phone && <Alert variant={'danger'}>{errors.phone} </Alert>}
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='meter'>MEDIDOR</InputGroup.Text>
                                <Form.Select

                                    name='meter_number'
                                    value={claims.meter_number}

                                    onChange={handlerChange}
                                >
                                    <option>Selecciona Medidor</option>
                                    {meters.map((meter) => <option key={meter.meter_id} value={meter.number_meter}>{meter.number_meter}</option>)}

                                </Form.Select>
                            </InputGroup>
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='direct'>DIRECCIÓN</InputGroup.Text>
                                <FormControl
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='direction'
                                    value={claims.direction}

                                    onChange={handlerChange}
                                />
                            </InputGroup>
                            {errors.direction && <Alert variant={'danger'}>{errors.direction} </Alert>}
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='borderLeft-M'>DESCRIPCIÓN</InputGroup.Text>
                                <FormControl
                                    as='textarea'
                                    aria-label="With textarea"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='descriptions'
                                    style={{ height: '150px' }}

                                    onChange={handlerChange}
                                    value={claims.descriptions}
                                />
                            </InputGroup>
                            {errors.descriptions && <Alert variant={'danger'}>{errors.descriptions} </Alert>}

                            <>
                                <h>La confirmación de la ubicación es opcional si lo requiere el socio para una mejor presición para el plomero.</h>
                                
                                <InputGroup>
                                    <InputGroup.Text>CONFIRMAR UBICACIÓN</InputGroup.Text>
                                    <Button  onClick={handleShow}>Geolocalización</Button>

                                </InputGroup>
                                <Modal show={show} onHide={handleClose}>
                                    <Modal.Header closeButton>
                                        <Modal.Title className='titleMod'>VERIFICACIÓN UBICACIÓN</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>



                                    <Maps resPosition={positions} />

                                    </Modal.Body>
                                    <Modal.Footer>

                                        <Button className='buttonConf' onClick={handlerMaps}>Confirmar</Button>

                                    </Modal.Footer>

                                </Modal>
                            </>
                        </FormGroup>
                        <Form className='FormBody' onSubmit={handlerSubmit}>
                            <Button className='buttonCre' onClick={handlerBlur} variant='primary' type='submit'>
                                Crear
                            </Button>
                            <Button className='buttonCan' variant='danger' href='/' >
                                Cancelar
                            </Button>
                        </Form>

                    </Card.Body>
                </Col>
            </Row>
        </Container>
    )
}

export default CreateClaim

