import React from 'react'
import Maps from './Maps';
import { Sesion } from '../../Hook/User/Sesion';
import { ShowModal } from '../../Hook/MapAndShow/ShowModal';
import '../../Styles/styleCard.css'
import { Card, ListGroup, ListGroupItem, Button, Modal } from 'react-bootstrap';
import { color, getDate, showComment } from '../../function/CssAndViewShow'
import { permissionPlumber } from '../../function/permissionShow';
import { ClaimInspectionData } from '../../Hook/MapAndShow/ClaimInspectionData';
function CardClaim({ resClaim }) {

  const { position, onSubmit } = ClaimInspectionData(resClaim);
  const { userSesion } = Sesion();
  const { show, handleShow, handleClose } = ShowModal();



  return (
    < Card className='cardClaim' style={color(resClaim.states)} >
      <Card.Header ><h5 className='cardHeader'>{resClaim.states}</h5></Card.Header>
      <ListGroup>
        <ListGroupItem>
          <span className='textTitle'>Socio:</span>
          <span> </span>
          <span>{resClaim.socio.partner_name + " " + resClaim.socio.surname_father + " " + resClaim.socio.surname_mother}</span>

        </ListGroupItem>
        <ListGroupItem>
          <span className='textTitle'>Fecha:</span>
          <span> </span>
          <span>{getDate(resClaim.claims_date)}</span>

        </ListGroupItem>
        <ListGroupItem>
          <span className='textTitle'>Telf.:</span>
          <span> </span>
          <span>{resClaim.phone}</span>

        </ListGroupItem>
        <ListGroupItem>
          <span className='textTitle'>Medidor:</span>
          <span> </span>
          <span>{resClaim.meter_number}</span>

        </ListGroupItem>
        <ListGroupItem>
          <span className='textTitle'>Dirección:</span>
          <span> </span>
          <span>{resClaim.direction}</span>

        </ListGroupItem>

        <Card.Body className='cardClaimBody'>
          <Card.Title className='textTitle'>Descripción</Card.Title>
          <Card.Text>{resClaim.descriptions}
          </Card.Text>
        </Card.Body>
        {
          showComment(resClaim.comment) ? <><ListGroupItem>
            <span className='textTitle'>Detalle:</span>
            <span> </span>
            <span>{resClaim.comment}</span>

          </ListGroupItem></> : <></>

        }
        {
          permissionPlumber(userSesion, resClaim.inspeccion) ?
            <>
              <ListGroupItem className='text-center'>
                <h6 className='textTitle'>MOSTRAR UBICACIÓN</h6>

                <Button className='buttonConf' onClick={handleShow}>Ver</Button>


                <Modal show={show} onHide={handleClose}>
                  <Modal.Header closeButton>
                    <Modal.Title className='titleMod'>UBICACIÓN RECLAMO</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>


                    <Maps resPosition={position} />


                  </Modal.Body>


                </Modal>
              </ListGroupItem>
              <ListGroupItem className='text-center'>

                <Button className='buttonInfo' variant='outline-info' type='Submit' onClick={() => onSubmit()}>Informe Inspección</Button>
              </ListGroupItem></> : <></>
        }
      </ListGroup>
    </Card >
  )
}

export default CardClaim

