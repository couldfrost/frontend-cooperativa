import React from 'react'
import { AuthenticPartnerForm } from '../../Hook/Forms/AutenticPartnerForm'
import SearchPartner from './SearchPartner';

function AuthenticPartner() {
    const authenticPart = {
        ci: ''
    };
    return (
        <SearchPartner res={AuthenticPartnerForm(authenticPart,"CREATE")}/>
        
    )
}

export default AuthenticPartner
