import React from 'react'
import CardClaim from './CardClaim';
import { Form, Alert, Container, Row, Col, ListGroup, Card, InputGroup, FormGroup, FormControl, Button } from 'react-bootstrap';
import Paginacion from '../Paginacion';
import 'bootstrap/dist/css/bootstrap.min.css'
import '../../Styles/styleSearch.css'
function ShowClaims({ res, menssage, sesion }) {
   
    return (
        <Container className='ContBodyInf'>
            <Row>
                <Col>
                    {menssage?.state?.response && <Alert variant='success' >{menssage.state.response}</Alert>}
                    <h3 className='title'>MOSTRAR RECLAMOS</h3>
                    {sesion ? <>
                        <Card.Body>
                            <FormGroup>
                                <InputGroup className="mb-3">
                                    <InputGroup.Text id="inputGroup-sizing-default" className='searchB'>BUSCAR</InputGroup.Text>
                                    <div>
                                        <Form.Select

                                            name='selection'
                                            value={res.selection}
                                            onChange={res.onSelectionChange}

                                        >
                                            <option >SELECCIONAR</option>
                                            <option value="MEDIDOR">MEDIDOR</option>
                                            <option value="CI">CI</option>

                                        </Form.Select>
                                    </div>
                                    <FormControl
                                        name='search'
                                        value={res.search}
                                        aria-label="DefaultPar"
                                        aria-describedby="inputGroup-sizing-default"
                                        onChange={res.onSearchChange}

                                    />

                                    <Button variant='primary' type='submit' onClick={res.onSubmit}>
                                        Confirmar
                                    </Button>
                                </InputGroup>
                                {res.errors.claims && <Alert variant={'danger'}>{res.errors.claims} </Alert>}
                            </FormGroup>
                        </Card.Body></> : <></>
                    }
                    <ListGroup>
                        {

                            res.currentClaim.map(claim => <CardClaim key={claim.claims_id} resClaim={claim} />)

                        }

                        <Paginacion claimsPerPage={res.claimsPerPage} totalClaims={res.claims.length} paginate={res.paginate} />

                       
                    </ListGroup>



                </Col>

            </Row>
        </Container>
    )
}

export default ShowClaims
