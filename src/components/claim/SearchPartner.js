import React from 'react'
import { Alert, Container, Button, Col, Card, FormGroup, Form, InputGroup, FormControl } from 'react-bootstrap';
function SearchPartner({ res }) {

    return (
        <Container className='ContBodyCret'>
            <Col md={{ span: 10, offset: 1 }}>
                <Card.Body>
                    {res.errors.authentic && <Alert variant={'danger'}>{res.errors.authentic} </Alert>}
                    <h3 className='title'>Confirmar Socio</h3>
                    <FormGroup>
                        <InputGroup className="mb-3">
                            <InputGroup.Text id="inputGroup-sizing-default">CI</InputGroup.Text>
                            <FormControl
                                name='ci'
                                value={res.authentic.ci}
                                aria-label="DefaultPar"
                                aria-describedby="inputGroup-sizing-default"

                                onChange={res.handlerChange}
                            />
                        </InputGroup>
                        {res.errors.ci && <Alert variant={'danger'}>{res.errors.ci} </Alert>}
                    </FormGroup>
                    <Form onSubmit={res.handlerSubmit}>
                        <Button className='buttonConf' variant='primary' type='submit'>
                            Confirmar
                        </Button>
                    </Form>
                </Card.Body>
                <h5 className='title'>Instrucciónes para crear reclamos</h5>
                <div className="ratio ratio-16x9">
                    
                    <iframe src="https://drive.google.com/file/d/1wuJVbg_P0tRltPo-axuF7SOaHtOuaZnX/preview" title="Vimeo video" width="640" height="480" allow="autoplay"></iframe>
                </div>

            </Col>


        </Container>
    )
}

export default SearchPartner
