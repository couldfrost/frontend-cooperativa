import React from 'react'
import '../../Styles/styleTabla.css'
import ShowClaim from './ShowClaim'
import ShowWork from './ShowWork'
import { color } from '../../function/CssAndViewShow'

function TableHistory({ resHistory }) {
    return (
        <tr className='word' style={color(resHistory.states)}>
            <td >{resHistory.socio.ci}</td>
            <td >{resHistory.claims_date}</td>
            <td>{resHistory.states}</td>
            <td><ShowClaim ClaimHistory={resHistory} /> </td>
            <td>{resHistory.trabajo_realizado && <ShowWork idWorkDone={resHistory.trabajo_realizado.work_done_id} />}</td>


        </tr>
    )
}

export default TableHistory
