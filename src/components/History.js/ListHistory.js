import React from 'react'
import '../../Styles/styleSearch.css';
import TableHistory from './TableHistory';
import { Container, Row, Col,Card,InputGroup,Form,Button,FormControl,FormGroup } from 'react-bootstrap';
import { MDBTable, MDBTableHead, MDBTableBody } from 'mdb-react-ui-kit';
import { SearchHistory } from '../../Hook/Searchs/SearchHistory';
function ListHistory() {
    const reqs={month:' ',year:' '}
    const{claims,search, handlerChange, onSubmit}=SearchHistory(reqs);
    

    return (
        <Container className='ContBody'>
            <Row>
                <Col>

                    <h3 className='title'>HISTORIAL</h3>
                    <Card.Body>
                        <FormGroup>
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='searchH'>BUSCAR</InputGroup.Text>
                                
                                <div>
                                <Form.Select
                                    
                                    name='month'
                                    value={search.month}
                                    onChange={handlerChange}

                                >
                                    <option >MES</option>
                                    <option value={1}>ENERO</option>
                                    <option value={2}>FEBRERO</option>
                                    <option value={3}>MARZO</option>
                                    <option value={4}>ABRIL</option>
                                    <option value={5}>MAYO</option>
                                    <option value={6}>JUNIO</option>
                                    <option value={7}>JULIO</option>
                                    <option value={8}>AGOSTO</option>
                                    <option value={9}>SEPTIEMBRE</option>
                                    <option value={10}>OCTUBRE</option>
                                    <option value={11}>NOVIEMBRE</option>
                                    <option value={12}>DICIEMBRE</option>
                                    
                                </Form.Select>
                                </div>

                                <FormControl
                                    name='year'
                                    value={search.year}
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    onChange={handlerChange}

                                />
                                <Button variant='primary' type='submit' onClick={onSubmit}>
                                    Confirmar
                                </Button>
                            </InputGroup>
                        </FormGroup>
                    </Card.Body>
                    <MDBTable responsive>
                        <MDBTableHead className='table-info'>
                            <tr>
                                <th scope='col'>CI</th>
                                <th scope='col'>FECHA</th>
                                <th scope='col'>ESTADO</th>
                                <th scope='col'>RECLAMO</th>
                                <th scope='col'>INFORME</th>
                            </tr>
                        </MDBTableHead>
                        <MDBTableBody>

                            {


                               claims.map(claim => <TableHistory key={claim.claims_id} resHistory={claim} />)

                            }

                        </MDBTableBody>
                    </MDBTable>
                </Col>
            </Row>
        </Container>
    )
}

export default ListHistory
