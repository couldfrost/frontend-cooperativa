import React from 'react'
import { Modal,Button } from 'react-bootstrap'
import CardClaim from '../claim/CardClaim';
import { ShowModal } from '../../Hook/MapAndShow/ShowModal';
function ShowClaim({ClaimHistory}) {
  const {show,handleClose,handleShow}=ShowModal();
  
  return (
    <>
    <Button variant='light' onClick={handleShow}>Mostrar</Button>

    <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
            <Modal.Title>RECLAMO</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <CardClaim resClaim={ClaimHistory}/>
        </Modal.Body>

    </Modal>
</>
  )
}

export default ShowClaim
