import React from 'react'
import { Modal, Button } from 'react-bootstrap'
import CardWorkDone from '../workDone/CardWorkDone';
import { ShowModal } from '../../Hook/MapAndShow/ShowModal';
import { GetOneWorkDone } from '../../Hook/Gets/GetOneWorkDone';

function ShowWork({ idWorkDone }) {
    const { show, handleClose, handleShow } = ShowModal();
    const { work } = GetOneWorkDone(idWorkDone);

    return (
        <>
            <Button variant='light' onClick={handleShow}>Mostrar</Button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title >INFORME DE TRABAJO</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <CardWorkDone resCard={work} />
                </Modal.Body>

            </Modal>
        </>
    )
}

export default ShowWork
