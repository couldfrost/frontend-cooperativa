import React from 'react'
import { Nav, NavDropdown } from 'react-bootstrap';
import { Sesion } from '../../Hook/User/Sesion';
import { permissionAdmin, permissionSecretary } from '../../function/permissionShow';
function BarSesion() {
    const { userSesion } = Sesion();

    return (
        <Nav>
            {permissionAdmin(userSesion) ? <>
                <NavDropdown title="Usuario" id="basic-nav-dropdown">

                    <NavDropdown.Item href="/ListUsers">Lista de Usuarios</NavDropdown.Item>

                </NavDropdown>
                <NavDropdown title="Detalles" id='basic-nav-dropdown'>
                    <NavDropdown.Item href="/ListHistory">Historial</NavDropdown.Item>
                    <NavDropdown.Item href="/Map">Ubicaciones en Mapa</NavDropdown.Item>
                </NavDropdown>
            </>
                : <></>

            }
            {permissionSecretary(userSesion) ?<>

                <NavDropdown title="Socios" id="basic-nav-dropdown">
                    <NavDropdown.Item href="/CreatePartner">Crear Socio</NavDropdown.Item>
                    <NavDropdown.Item href="/ListPartners">Mostrar Socios</NavDropdown.Item>

                </NavDropdown>
                <NavDropdown title="Detalles" id='basic-nav-dropdown'>
                    <NavDropdown.Item href="/ListHistory">Historial</NavDropdown.Item>
                    <NavDropdown.Item href="/Map">Ubicaciones en Mapa</NavDropdown.Item>
                </NavDropdown>
                </>
                : <></>
            }

            <NavDropdown title="Informes" id="basic-nav-dropdown">
                <NavDropdown.Item href="/ListInspections">Mostrar informes de inspección</NavDropdown.Item>
                <NavDropdown.Item href="/ListWorkDone">Mostrar informes de trabajo realizados</NavDropdown.Item>

            </NavDropdown>
            

        </Nav>
    )
}

export default BarSesion
