import React from 'react'
import {
  MDBFooter,
  MDBContainer,
  MDBCol,
  MDBRow,
} from 'mdb-react-ui-kit';
import { Image } from 'react-bootstrap'
function Footer() {
  return (
    <MDBFooter bgColor='primary' className='text-white text-center text-lg-left'>
      <MDBContainer className='p-4'>
        <MDBRow>
          <MDBCol lg='6' md='12' className='mb-4 mb-md-0'>
            <h5 className='text-uppercase'>COOPERATIVA Llauquinquiri </h5>

            <p>
              Encargada de distribuir agua potable a los socios de la
              zona Llauquinquiri, ayudando a que este líquido vital llegue a sus hogares
              y también para el riegue de sus cultivos
            </p>
          </MDBCol>

          <MDBCol className='mb-4 mb-md-0'>
            <h5 className='text-uppercase mb-0'>CONTACTOS</h5>

            <ul className='list-unstyled'>
              <li>
                <a className='text-white'>
                  <Image className='imghome' src='icons/icons8-whatsapp-64.png' /> 72744939
                </a>
              </li>
              <li>
                <a className='text-white'>
                  <Image className='imghome' src='icons/icons8-gmail-48.png' /> cooperativadeaguaco@gmail.com
                </a>
              </li>
              <li>
                <a className='text-white'>
                  Ultima actualización: 11/08/2023
                </a>
              </li>

            </ul>
          </MDBCol>
        </MDBRow>
      </MDBContainer>

      <div className='text-center p-3' style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
        &copy; {new Date().getFullYear()} Copyright:{' '}
        <p className='text-white' >
          Todos los Derechos Reservados
        </p>
      </div>
    </MDBFooter>
  )
}

export default Footer
