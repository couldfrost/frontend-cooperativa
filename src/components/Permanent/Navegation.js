import React from 'react'
import '../../Styles/styleNavigate.css'
import BarSesion from './BarSesion';
import BarLogin from './BarLogin';
import { Sesion } from '../../Hook/User/Sesion';
import { Nav, Navbar, NavDropdown, Container, Button, Image } from 'react-bootstrap';
function Navegation() {
    const { userSesion } = Sesion();


    return (

        <Navbar bg="primary" variant="dark" expand="lg" >
            <Container >

                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto">
                        <Nav.Link href="/"><Image className='imghome' src='icons/icons8-casa-48.png' />Inicio</Nav.Link>
                        {
                            userSesion ? <BarSesion /> : <></>
                        }
                        <NavDropdown title="Reclamos" id="basic-nav-dropdown">
                            <NavDropdown.Item href="/AuthenticPartner">Crear Reclamo</NavDropdown.Item>
                            {
                                userSesion ? <>
                                    <NavDropdown.Item href="/ListClaims">Mostrar Reclamos</NavDropdown.Item>
                                </> : <>
                                    <NavDropdown.Item href="/AuthenticShowPartner">Ver Reclamos</NavDropdown.Item>
                                </>
                            }


                        </NavDropdown>

                    </Nav>

                </Navbar.Collapse>
                <Navbar.Collapse className="justify-content-end">
                    {
                        userSesion ? <BarLogin /> :
                            <Button variant='light' href='/Login' >Inicio Sesión</Button>

                    }
                </Navbar.Collapse>
            </Container>
        </Navbar>

    )
}

export default Navegation
