import React from 'react'
import { Nav, NavDropdown } from 'react-bootstrap';
import { Sesion } from '../../Hook/User/Sesion';
import { permissionDirective } from '../../function/permissionShow';
function BarLogin() {


    const { removeSesion, userSesion } = Sesion();
    
    return (
        
        <Nav>
            
            <NavDropdown title={userSesion.userEmail} id="basic-nav-dropdown">
                {permissionDirective(userSesion) &&
                <NavDropdown.Item href='/ForgotPassword'>Cambiar Cuenta</NavDropdown.Item>
                }
                <NavDropdown.Item onClick={removeSesion} href="/">Cerrar Sesión</NavDropdown.Item>


            </NavDropdown>
        </Nav>
    )
}

export default BarLogin
