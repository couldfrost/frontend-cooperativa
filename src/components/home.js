import React from 'react'
import '../Styles/styleHome.css'
import Carousels from './Carousels'
import { Container, Row, Col, Card } from 'react-bootstrap'

function home() {
  return (
    <div>
      <Carousels />
      
        <Container className='CardHomeM'>


          <Row>
            <Col className='cardcol'>
              <Card className='cardHome' border='primary'  >
                <Card.Header className='cardhear'>MISION</Card.Header>
                <Card.Body className='cardClaimBody'>
                  <Card.Text >
                    Prestar servicios de Agua Potable y Alcantarillado Sanitario a la población dentro del área de licencia con calidad, continuidad y eficiencia para mejorar sus condiciones de vida.
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col className='cardcol'>
              <Card border='primary' className='cardHome'>
                <Card.Header className='cardhear'>VISION</Card.Header>
                <Card.Body className='cardClaimBody'>
                  <Card.Text >
                    Ser una cooperativa modelo en el area periurbana, por su eficiencia, innovación tecnologíca, personal capacitado, referente nacional en la prestacion de los servicios.
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      
    </div>
  )
}

export default home
