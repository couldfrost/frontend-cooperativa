import React from 'react'
import '../../Styles/styleInspeccion.css'
import { InspectionForm } from '../../Hook/Forms/InspectionForm'
import { Alert,Container, Button, Col, Card, FormGroup, Form, InputGroup, FormControl, Row } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';
function CreateInspection() {
    const location = useLocation();
    const Cid=location.state.claim.claims_id;
    const name=location.state.claim.socio.partner_name;
    const father=location.state.claim.socio.surname_father;
    const mother=location.state.claim.socio.surname_mother;
    const namePartner=name+" "+father+" "+mother;
    
    
    const inspection = {
        claims_id: Cid, 
        name_plumber: '',
        description: '',
        inspections_date: new Date(),        
        name_pay: namePartner
    }
    
    const {
        inspections, errors, handlerChange, handlerBlur, handlerSubmit,
    } = InspectionForm(inspection)
    
   
    return (
        <Container className='ContBodyCret'>
            <Row>
                <Col md={{ span: 10, offset: 1 }}>
                    <Card.Body>
                        <h3 className='title'>CREAR INFORME DE INSPECCIÓN</h3>
                        <FormGroup className='bodyP'>

                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='Plumber'>PLOMERO</InputGroup.Text>
                                <FormControl
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='name_plumber'
                                    value={inspections.name_plumber}
                                    
                                    onChange={handlerChange}
                                />
                            </InputGroup>
                            {errors.name_plumber && <Alert variant={'danger'}>{errors.name_plumber} </Alert>}
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='borderLeft-M'>DESCRIPCIÓN</InputGroup.Text>
                                <FormControl
                                    as='textarea'
                                    aria-label="With textarea"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='description'
                                    style={{ height: '150px' }}
                                    onChange={handlerChange}
                                    
                                    value={inspections.description}
                                />
                            </InputGroup>
                            {errors.description && <Alert variant={'danger'}>{errors.description} </Alert>}


                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='dateC'>FECHA</InputGroup.Text>
                                <FormControl
                                    placeholder={new Date().toLocaleDateString()}
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    disabled
                                />
                            </InputGroup>

                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='borderLeft-FS'>SOCIO</InputGroup.Text>
                                <FormControl
                                    placeholder={namePartner}
                                    aria-label="Default select example"
                                    aria-describedby="inputGroup-sizing-default"
                                    disabled
                                    
                                >
                                </FormControl>
                            </InputGroup>
                            {errors.name_pay && <Alert variant={'danger'}>{errors.name_pay} </Alert>}

                        </FormGroup>
                        <Form className='FormBody' onSubmit={handlerSubmit} >
                            <Button className='buttonCre' onClick={handlerBlur} variant='primary' type='submit'>
                                Crear
                            </Button>
                            <Button className='buttonCan'  variant='danger' href='/ListClaims'>
                                Cancelar
                            </Button>
                        </Form>

                    </Card.Body>
                </Col>
            </Row>
        </Container>
    )
}

export default CreateInspection
