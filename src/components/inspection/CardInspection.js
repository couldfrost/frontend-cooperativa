import React from 'react'
import '../../Styles/styleCard.css'
import { Alert, Card, ListGroup, ListGroupItem, Form, FormControl, Button } from 'react-bootstrap';
import { ApproveInspectionUpdte } from '../../Hook/Forms/ApproveInspectionUpdate';
import { permissionDirective, permissionPlumber } from '../../function/permissionShow'
import { getDate,color} from '../../function/CssAndViewShow'
import { Sesion } from '../../Hook/User/Sesion';
function CardInspection({ resInspection }) {
  const { userSesion } = Sesion();
  const approve = {
    states: ' ',
    comment: ' '
  }
  const {
    approves, errors, handlerChange, handlerBlur, handlerSubmit, handlerSubmitWorkDone
  } = ApproveInspectionUpdte(approve, resInspection);



  return (
    < Card className='cardClaim' style={color(resInspection.reclamo.states)}>
      <Card.Header ><h5 className='cardHeader'>{resInspection.reclamo.states}</h5></Card.Header>
      <ListGroup>
        <ListGroupItem>
          <span className='textTitle'> Plomero:</span>
          <span>   </span>
          <span>{resInspection.name_plumber}</span>
        </ListGroupItem>

        <ListGroupItem>
          <span className='textTitle'>Fecha:</span>
          <span> </span>
          <span>{getDate(resInspection.inspections_date)}</span>
          
        </ListGroupItem>

        <ListGroupItem>
          <span className='textTitle'>N° Contacto:</span>
          <span> </span>
          <span>{resInspection.reclamo.phone}</span>
          
        </ListGroupItem>
        <ListGroupItem>
          <span className='textTitle'>Socio:</span>
          <span> </span>
          <span>{resInspection.name_pay}</span>
          
        </ListGroupItem>

        <Card.Body className='cardClaimBody'>
          <Card.Title className='textTitle'>Descripción</Card.Title>
          <Card.Text>{resInspection.description}
          </Card.Text>
        </Card.Body>
        {
          permissionDirective(userSesion, null, resInspection.reclamo.states) ?
            <ListGroupItem className='probe'>
              <h4 className='textTitle'>APROBAR</h4>
              <h6>Estado</h6>
              <Form.Select

                name='states'
                value={approves.states}
                onChange={handlerChange}
              >
                <option value="PENDIENTE">PENDIENTE</option>
                <option value="ACEPTADO">ACEPTADO</option>
                <option value="CANCELADO">CANCELADO</option>

              </Form.Select>
              <h6>Detalle</h6>
              <FormControl
                as='textarea'
                aria-label="With textarea"
                aria-describedby="inputGroup-sizing-default"

                name='comment'
                style={{ height: '150px' }}
                onChange={handlerChange}
                onBlur={handlerBlur}
                value={approves.comment}

              />
              {errors.comment && <Alert variant={'danger'}>{errors.comment} </Alert>}
              <p></p>
              <Form onSubmit={handlerSubmit}>
                <Button className='buttonConf' variant='warning' type='Submit'>Aceptar</Button>
              </Form>
            </ListGroupItem> : <></>

        }
        {
          permissionPlumber(userSesion, resInspection.trabajo_realizado, resInspection.reclamo.states) ?
            <ListGroupItem className='text-center'>
              <Button className='buttonInfo' variant='outline-info' type='Submit' onClick={() => handlerSubmitWorkDone()}> Informe de trabajo</Button>
            </ListGroupItem> : <></>
        }
      </ListGroup>
    </Card >
  )
}

export default CardInspection
