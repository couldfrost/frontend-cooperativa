import React from 'react'
import CardInspection from './CardInspection';
import '../../Styles/styleInformation.css'
import { Alert, Container, Row, Col, ListGroup } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';
import { GetInspections } from '../../Hook/Gets/GetInspections';
import Paginacion from '../Paginacion';
function ListInspections() {
  const location = useLocation();
  const { inspections, paginate, currentInspections, inspectionsPerPage } = GetInspections();

  return (
    <Container className='ContBodyInf'>
      <Row>
        <Col>
          {location.state ? <Alert variant='success' >{location.state.response}</Alert> : <></>}
          <h3 className='title'>MOSTRAR INFORMES DE INSPECCIÓN</h3>

          <ListGroup>
            {

              currentInspections.map(inspection => <CardInspection key={inspection.inspections_id} resInspection={inspection} />)

            }
            <Paginacion claimsPerPage={inspectionsPerPage} totalClaims={inspections.length} paginate={paginate} />
          </ListGroup>
        </Col>
      </Row>
    </Container>
  )
}

export default ListInspections
