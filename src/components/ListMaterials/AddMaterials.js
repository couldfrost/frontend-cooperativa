import React from 'react'
import { Alert,Modal, Button, InputGroup, FormControl } from 'react-bootstrap'
import { MaterialForm } from '../../Hook/Forms/MaterialForm';
function AddMaterials() {
    
    const material={
        material_name: ''
    };
    
    const {
        materials,show, errors, handlerChange, handlerBlur, handlerSubmit,handleClose,handleShow
    }=MaterialForm(material);
       
    return (
        <>
        <Button variant='warning' onClick={handleShow}>Agregar Articulo</Button>
                                
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Agregar Nuevo Articulo</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <InputGroup className="mb-3">
                    <InputGroup.Text id="inputGroup-sizing-default" className='borderLeft-D'>ARTICULO</InputGroup.Text>
                    <FormControl
                        aria-label="Default select example"
                        aria-describedby="inputGroup-sizing-default"

                        name='material_name'
                        onBlur={handlerBlur}
                        onChange={handlerChange}
                        value={materials.material_name}
 
                    >
                    </FormControl>
                </InputGroup>
                {errors.material_name && <Alert variant={'danger'}>{errors.material_name} </Alert>}
                {errors.material && <Alert variant={'danger'}>{errors.material} </Alert>}
            </Modal.Body>
            <Modal.Footer>
                
                    <Button className='buttonConf' onClick={handlerSubmit} >Agregar</Button>
                 
            </Modal.Footer>
        </Modal>
        </>
    )
}

export default AddMaterials
