import React from 'react'
import '../../Styles/styleListMaterial.css'
import { Alert, Container, Button, Col, Card, FormGroup, Form, InputGroup, FormControl, Row } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';
import ListMaterials from './ListMaterials';
import AddMaterials from './AddMaterials';
import { ListMaterialForm } from '../../Hook/Forms/ListMaterialForm';
import { GetMaterials } from '../../Hook/Gets/GetMaterials';
import { GetOneWorkDoneForIdIspection } from '../../Hook/Gets/GetOneWorkDoneForIdIspection';
function CreateListMaterials() {
    const location = useLocation();
    const idInspection = location.state.Iid;
    const {materials}=GetMaterials();
    const {workDone,listMaterials}=GetOneWorkDoneForIdIspection(idInspection);
    const addMaterial = {
        article: ' ',
        amount: ' ',
        observation: ' '

    };
    const {
        addMaterials, errors,onSubmit, handlerChange, handlerBlur, handlerSubmit,
    } = ListMaterialForm(addMaterial, workDone);

    return (
        <Container className='ContBodyCret'>
            <Row>
                <Col md={{ span: 10, offset: 1 }}>
                    <Card.Body>
                        <h3 className='title'>CREAR LISTA DE MATERIALES</h3>
                        <FormGroup className='bodyP'>
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='article'>ARTICULO</InputGroup.Text>
                                <Form.Control
                                    as="select"
                                    htmlSize={3}
                                                          
                                    name='article'
                                    value={addMaterials.article}
                                    onChange={handlerChange}                                    
                                    className="select"                            >
                                    <option>Seleccionar</option>
                                    {materials.map((material) => <option key={material.material_id} value={material.material_name}>{material.material_name}</option>)}

                                </Form.Control>

                                <AddMaterials />
                            </InputGroup>

                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='cant'>CANTIDAD</InputGroup.Text>
                                <FormControl
                                    aria-label="Default select example"
                                    aria-describedby="inputGroup-sizing-default"

                                    name='amount'
                                    onChange={handlerChange}

                                    value={addMaterials.amount}

                                >
                                </FormControl>
                            </InputGroup>
                            {errors.amount && <Alert variant={'danger'}>{errors.amount} </Alert>}
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='obser'>OBSERVACIÓN</InputGroup.Text>
                                <FormControl
                                    aria-label="Default select example"
                                    aria-describedby="inputGroup-sizing-default"

                                    name='observation'

                                    onChange={handlerChange}
                                    value={addMaterials.observation}

                                >
                                </FormControl>
                            </InputGroup>
                            {errors.observation && <Alert variant={'danger'}>{errors.observation} </Alert>}
                        </FormGroup>
                        <Form className='FormBody' onSubmit={handlerSubmit}>
                            <Button className='buttonConf' onClick={handlerBlur} variant='primary' type='submit'>
                                Añadir
                            </Button>
                        </Form>

                    </Card.Body>
                    <ListMaterials ress={listMaterials} />
                    <div className='FormBody'>
                        <h5>Terminar Informe  </h5>
                        <Button className='buttonConf' variant='primary' onClick={onSubmit}>
                            Confirmar
                        </Button>
                    </div>
                </Col>
            </Row>
        </Container>
    )
}

export default CreateListMaterials
