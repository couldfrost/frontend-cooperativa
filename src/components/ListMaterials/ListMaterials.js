import React from 'react'
import TableListMaterials from './TableListMaterials';
import { MDBTable, MDBTableHead, MDBTableBody } from 'mdb-react-ui-kit';
function ListMaterials({ress}) {
  return (
    <MDBTable responsive>
                        <MDBTableHead className='table-info'>
                            <tr>
                                <th scope='col'>ARTICULO</th>
                                <th scope='col'>CANTIDAD</th>
                                <th scope='col'>OBSERVACIÓN</th>
                                                             
                                
                            </tr>
                        </MDBTableHead>
                        <MDBTableBody>

                            {

                                ress.map(res => <TableListMaterials key={res.list_material_id} res_ListMaterial={res} />)

                            }

                        </MDBTableBody>
                    </MDBTable>
  )
}

export default ListMaterials
