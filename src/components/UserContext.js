import React,{createContext, useState} from "react";
export const UserContext=createContext();

export const UserProvider=({children})=>{
    const [user,setUser]= useState(null);
    
    const userSesion=user;
    const setUserSesion=(user)=>{
       
        setUser(JSON.parse(user));
        localStorage.setItem("UserSesion",user)
    }
    const removeSesion=()=>{
         setUser(null);
         localStorage.removeItem("UserSesion")     
    }
    return(
        <UserContext.Provider value={{userSesion,setUserSesion,removeSesion}}>
            {children}
        </UserContext.Provider>
    )
}