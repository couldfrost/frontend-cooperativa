import React from 'react'
import '../../Styles/styleUser.css'
import { Alert, Container, Button, Col, Card, FormGroup, Form, InputGroup, FormControl, Row } from 'react-bootstrap';
function FormUser({ res }) {
    return (
        <Container className='ContBodyCret'>
            <Row>
                <Col md={{ span: 10, offset: 1 }}>
                    <Card.Body>
                        {
                            res.showUpdate() ? <>
                                {res.errors.user && <Alert variant={'danger'}>{res.errors.user} </Alert>}
                                <h3 className='title'>CAMBIAR CUENTA</h3></>

                                : <h3 className='title'>CAMBIAR CONTRASEÑA</h3>
                        }
                        <FormGroup className='bodyP'>
                            {
                                res.showUpdate() ? <>
                                    <InputGroup className="mb-3">
                                        <InputGroup.Text id="inputGroup-sizing-default" className='nameU'>NOMBRE</InputGroup.Text>
                                        <FormControl
                                            aria-label="DefaultPar"
                                            aria-describedby="inputGroup-sizing-default"
                                            name='name'
                                            value={res.users.name}
                                            onChange={res.handlerChange}

                                        />
                                    </InputGroup>

                                    {res.errors.name && <Alert variant={'danger'}>{res.errors.name} </Alert>}
                                   </> : <></>
                            }
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='passU'>PASSWORD</InputGroup.Text>
                                <FormControl
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='password'
                                    value={res.users.password}
                                    type="password"
                                    onChange={res.handlerChange}

                                />
                            </InputGroup>
                            {res.errors.password && <Alert variant={'danger'}>{res.errors.password} </Alert>}
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='RePassU'>CONFIRMAR PASSWORD</InputGroup.Text>
                                <FormControl
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='resPassword'
                                    value={res.users.resPassword}
                                    type="password"
                                    onChange={res.handlerChange}

                                />
                            </InputGroup>
                            {res.errors.resPassword && <Alert variant={'danger'}>{res.errors.resPassword} </Alert>}
                            
                        </FormGroup>
                        <Form className='FormBody' onSubmit={res.handlerSubmit}>
                            <Button className='buttonCre' onClick={res.handlerBlur} variant='primary' type='submit'>
                                {res.showUpdate() ? <>Registrar</> : <>Cambiar</>}
                            </Button>
                            <Button className='buttonCan' variant='danger' onClick={res.handleClose}>
                                Cancelar
                            </Button>
                        </Form>

                    </Card.Body>
                </Col>
            </Row>
        </Container>
    )
}

export default FormUser
