import React from 'react'

import OnlyPassword from './OnlyPassword';
function TableUsers({ ResUsers }) {
    
  
    return (
        <tr>
            <td>{ResUsers.rol}</td>
            <td>{ResUsers.email}</td>
            
            <td>

                <OnlyPassword id={ResUsers.user_id}/>

            </td>
    
        </tr>
    )
}

export default TableUsers
