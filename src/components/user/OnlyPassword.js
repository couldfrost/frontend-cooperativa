import React from 'react'
import { Modal, Button} from 'react-bootstrap'
import { ChangerPassword } from '../../Hook/User/ChangerPassword';
import FormUser from './FormUser';
import { ShowModal } from '../../Hook/MapAndShow/ShowModal';
function OnlyPassword({ id }) {
    const Npassword = {
        password: '',
        resPassword: ''
    }
   
    const {show,handleShow,handleClose}=ShowModal();
    return (
        <>
            <Button onClick={handleShow} variant="warning">Cambiar</Button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                
                </Modal.Header>
                <Modal.Body>
                    {/*
                    <FormGroup>
                        <InputGroup className="mb-3">
                            <InputGroup.Text id="inputGroup-sizing-default" className='pass'>PASSWORD</InputGroup.Text>
                            <FormControl
                                aria-label="DefaultPar"
                                aria-describedby="inputGroup-sizing-default"
                                name='password'
                                value={users.password}
                                type="password"
                                onChange={handlerChange}

                            />
                        </InputGroup>
                        {errors.password && <Alert variant={'danger'}>{errors.password} </Alert>}
                        <InputGroup className="mb-3">
                            <InputGroup.Text id="inputGroup-sizing-default" className='pass' >CONFIRMAR</InputGroup.Text>
                            <FormControl
                                aria-label="DefaultPar"
                                aria-describedby="inputGroup-sizing-default"
                                name='resPassword'
                                value={users.resPassword}
                                type="password"
                                onChange={handlerChange}


                            />
                        </InputGroup>
                        {errors.resPassword && <Alert variant={'danger'}>{errors.resPassword} </Alert>}
                    </FormGroup>

                    <Form className='FormBody' onSubmit={handlerSubmit}>
                        <Button className='buttonCre' onClick={handlerBlur} variant='primary' type='submit'>
                            Cambiar
                        </Button>
                        <Button className='buttonCan' variant='danger' onClick={handleClose} >
                            Cancelar
                        </Button>
                    </Form>
    */}
                    <FormUser res={ChangerPassword(Npassword, id,"ACCOUNT")} />
                </Modal.Body>

            </Modal>
        </>
    )
}

export default OnlyPassword
