import React from 'react';
import { Alert,Container, Button, Col, Card, FormGroup, Form, InputGroup, FormControl, Row } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css'
import '../../Styles/styleLogin.css' 
import {LoginForm} from '../../Hook/User/LoginForm'
function Login() {
    const sesion={
        email: '',
        password: ''
    }
    const {
        logins, errors, handlerChange, handlerSubmit
    } = LoginForm(sesion);
    return (
        <Container className='ContBodyCret'>
            <Row>
                <Col md={{ span: 10, offset: 1 }}>
                    <Card.Body>
                        <h3 className='title'>INICIO DE SESIÓN</h3>
                        <FormGroup className='bodyP'>
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='email'>EMAIL</InputGroup.Text>
                                <FormControl
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='email'
                                    value={logins.email}
                                    onChange={handlerChange}
                                    
                                />
                            </InputGroup>
                            
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='pass'>PASSWORD</InputGroup.Text>
                                <FormControl
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='password'
                                    value={logins.password}
                                    type="password"
                                    onChange={handlerChange}
                                    
                                />
                            </InputGroup>
                            {errors.user && <Alert variant={'danger'}>{errors.user} </Alert>}
                        </FormGroup>
                        <Form className='FormBody' onSubmit={handlerSubmit} >
                            <Button className='buttonCre' variant='primary' type='submit' >
                                Iniciar
                            </Button>
                            <Button className='buttonCan' variant='danger' >
                                Cancelar
                            </Button>
                        </Form>
                     
                    </Card.Body>
                </Col>
            </Row>
        </Container>
    )
}

export default Login
