import React from 'react'
import '../../Styles/styleUserList.css'
import { Container, Row, Col} from 'react-bootstrap';
import { MDBTable, MDBTableHead, MDBTableBody } from 'mdb-react-ui-kit';
import TableUsers from './TableUsers';
import { GetUsers } from '../../Hook/User/GetUsers';
function ListUsers() {
    const {Users}=GetUsers();
    return (
        <Container className='ContBody'>
            <Row className='RowM'>
                <Col>

                    <h3 className='title'>MOSTRAR USUARIOS</h3>
                    <MDBTable responsive>
                        <MDBTableHead className='table-info'>
                            <tr>
                                <th scope='col'>ROL</th>
                                <th scope='col'>EMAIL</th>
                                
                            </tr>
                        </MDBTableHead>
                        <MDBTableBody>

                            {

                               
                               Users.map(user=><TableUsers key={user.user_id} ResUsers={user}/> )

                            }

                        </MDBTableBody>
                    </MDBTable>
                </Col>
            </Row>
        </Container>
    )
}

export default ListUsers
