import React from 'react';
import { UserForm } from '../../Hook/User/UserForm';
import 'bootstrap/dist/css/bootstrap.min.css';
import  FormUser  from '../../components/user/FormUser';

function RegisterUser() {
    const user={
        name: '',
        email: '',
        password: '',
        resPassword: '',
        rol: ''
    }
  
 
    return (
               <FormUser res={UserForm(user,"REGISTER")}/>
    )
}

export default RegisterUser
