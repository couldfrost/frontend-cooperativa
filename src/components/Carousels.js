import React from 'react'
import { Carousel } from 'react-bootstrap'

function Carousels() {

    return (
        <div>
            <Carousel>
                <Carousel.Item>
                    <img className='imgCaro' src='img/grifo-abierto-de-agua-potable.jpg' alt='First slide' />
                </Carousel.Item>
                <Carousel.Item>
                    <img className='imgCaro' src='img/WhatsApp Image 2022-09-26 at 18.58.58.jpeg' alt='First slide' />
                </Carousel.Item>
                <Carousel.Item>
                    <img className='imgCaro' src='img/WhatsApp Image 2022-09-26 at 19.04.33.jpeg' alt='First slide' />
                </Carousel.Item>
            </Carousel>
        </div>
    )
}

export default Carousels
