import React, { useMemo } from 'react'
import '../../Styles/styleSearch.css';
import { SearchMap } from '../../Hook/Searchs/SearchMap';
import Mark from './Mark';
import credentials from '../../function/credentials';
import { Container,Alert, Row, Col, Card, InputGroup, Form, Button, FormControl, FormGroup } from 'react-bootstrap';
import { withScriptjs, withGoogleMap, GoogleMap } from "react-google-maps"
function MapeoClaims() {
  
  const reqs = { month: '', year: '' }
  const { ubications, errors, search, handlerChange, onSubmit } = SearchMap(reqs);
  
  const MyMapComponent = withScriptjs(withGoogleMap((props) =>
    <GoogleMap
      defaultZoom={14}

      defaultCenter={{ lat: -17.367585907929517, lng: -66.2527087834533 }}
    >
      {props.isMarkerShown && <div> {ubications.map(ubication => <Mark key={ubication.claims_id} res={ubication} />)}

      </div>}
    </GoogleMap>
  ));
  const map = useMemo(() => <MyMapComponent
    isMarkerShown
    googleMapURL={credentials.URL}
    loadingElement={<div style={{ height: `100%` }} />}
    containerElement={<div style={{ height: `400px` }} />}
    mapElement={<div style={{ height: `100%` }} />}
  />, [ubications]);
  return (
    <Container className='ContBody'>
      <Row>
        <Col>

          <h3 className='title'>UBICACIÓN DE RECLAMOS</h3>
          <Card.Body>
            <FormGroup>
              <InputGroup className="mb-3">
                <InputGroup.Text id="inputGroup-sizing-default" className='searchH'>BUSCAR</InputGroup.Text>

                <div>
                  <Form.Select

                    name='month'
                    value={search.month}
                    onChange={handlerChange}

                  >
                    <option >MES</option>
                    <option value={1}>ENERO</option>
                    <option value={2}>FEBRERO</option>
                    <option value={3}>MARZO</option>
                    <option value={4}>ABRIL</option>
                    <option value={5}>MAYO</option>
                    <option value={6}>JUNIO</option>
                    <option value={7}>JULIO</option>
                    <option value={8}>AGOSTO</option>
                    <option value={9}>SEPTIEMBRE</option>
                    <option value={10}>OCTUBRE</option>
                    <option value={11}>NOVIEMBRE</option>
                    <option value={12}>DICIEMBRE</option>

                  </Form.Select>
                </div>

                <FormControl

                  name='year'
                  value={search.year}
                  aria-label="DefaultPar"
                  aria-describedby="inputGroup-sizing-default"
                  onChange={handlerChange}
                  placeholder="AÑO"
                />
                <Button variant='primary' type='submit' onClick={onSubmit}>
                  Confirmar
                </Button>
              </InputGroup>
              {errors.Ubications && <Alert variant={'danger'}>{errors.Ubications} </Alert>}
            </FormGroup>
          </Card.Body>
          
          {
            ubications &&
            <div>
              {map}
            </div>
          }
        </Col>
      </Row>
    </Container>
  )
}

export default MapeoClaims
