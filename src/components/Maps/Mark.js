import React from 'react'
import { Marker, InfoWindow } from "react-google-maps"

import { ShowMark } from '../../Hook/MapAndShow/ShowMark'

function Mark({ res }) {


  const { selectedElement, setSelectedElement } = ShowMark()
  return (


    <Marker
      position={{ lat: res.lat, lng: res.lng }}
      onClick={() => setSelectedElement(res)}

    >

      {selectedElement ? <InfoWindow

        onCloseClick={() => setSelectedElement(null)}

      >
        <div>
          <h6>{res.descriptions}</h6>
          <h6 className='date_map'>{res.claims_date}</h6>
        </div>
      </InfoWindow> : null
      }
    </Marker>

  )
}

export default Mark
