import React, { useRef } from 'react'
import { Alert, Modal, Button, ListGroupItem, FormControl, InputGroup } from 'react-bootstrap'
import { WorkDoneUpdte } from '../../Hook/Forms/WorkDoneUpdate'
import CardWorkDone from './CardWorkDone'
import { permissionDirective, permissionSecretary, permissionPlumber2 } from '../../function/permissionShow'
import ReactToPrint from 'react-to-print'
import { GetUserPrint } from '../../Hook/User/GetUserPrint'
import { Sesion } from '../../Hook/User/Sesion'
function ShowWorkDone({ respon }) {
    const { userSesion } = Sesion();
    const updateWork = {
        repair_cost: '',
        stated: "CONFIRMADO"
    };
    const { userPrintP, userPrintT } = GetUserPrint();
    let componentRef = useRef();
    const { approves, show, errors, handlerChange, handlerBlur, handlerSubmit, handleShow, handleClose, handlerSubmitMaterials
    } = WorkDoneUpdte(updateWork, respon.work_done_id, respon);

    return (
        <>


            <Button variant='light' onClick={handleShow}>Ver</Button>

            <Modal show={show} onHide={handleClose}>
                {permissionSecretary(userSesion, null, respon.stated) &&
                    <ReactToPrint
                        trigger={() => {
                            return <Button>Imprimir</Button>
                        }}
                        content={() => componentRef}
                        documentTitle="new document"
                        pageStyle="print"
                    />
                }
                <div ref={(el) => (componentRef = el)}>
                    <Modal.Header closeButton>
                        <Modal.Title>INFORME DE TRABAJO</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <CardWorkDone resCard={respon} />
                        {permissionDirective(userSesion, null, respon.stated) ?
                            <ListGroupItem style={{ backgroundColor: '#B0D3FB' }} >
                                <h4 className='title'>APROBAR</h4>
                                <h6 className='titleMod'>Confirmar pago</h6>

                                <InputGroup>
                                    
                                    <FormControl
                                        aria-label="DefaultPar"
                                        aria-describedby="inputGroup-sizing-default"
                                        name='repair_cost'
                                        onBlur={handlerBlur}
                                        onChange={handlerChange}
                                        value={approves.repair_cost}


                                    />
                                    <InputGroup.Text id="inputGroup-sizing-default" >Bs.</InputGroup.Text>
                                </InputGroup>
                                {errors.repair_cost && <Alert variant={'danger'}>{errors.repair_cost} </Alert>}
                                <p />
                                <Button onClick={handlerSubmit} variant='success' type='Submit'>Aceptar</Button>

                            </ListGroupItem> : <></>
                        }
                        {permissionPlumber2(userSesion, null, respon.stated) &&
                            <ListGroupItem  >
                                <Button onClick={handlerSubmitMaterials} variant='success' type='Submit'>Crear Lista de Materiales</Button>
                            </ListGroupItem>
                        }
                    </Modal.Body>


                    {permissionSecretary(userSesion, null, respon.stated) &&
                        <Modal.Footer>
                            <div className='Presidente'>
                                <h6 >{userPrintP.name}</h6>

                                <h6 >Miembro Directiva a Cargo</h6>
                            </div>
                            <div className='Tesorera'>
                                <h6 >{userPrintT.name}</h6>

                                <h6 >Miembro Directiva a Cargo</h6>
                            </div>

                            <div>
                                <h6 className='textPres'>{respon.inspeccion.name_plumber}</h6>

                                <h6 className='textPlum'>PLOMERO</h6>
                            </div>
                        </Modal.Footer>
                    }

                </div>
            </Modal>

        </>
    )
}

export default ShowWorkDone
