import React from 'react'
import { Card, ListGroup, ListGroupItem } from 'react-bootstrap'
import ListMaterials from '../ListMaterials/ListMaterials'
import { color, getDate } from '../../function/CssAndViewShow'

function CardWorkDone({ resCard }) {
    return (
        < Card className='cardClaim' style={color(resCard.stated)}>

            <Card.Header ><h5 className='cardHeader'>{resCard.stated}</h5></Card.Header>
            <ListGroup>
                <ListGroupItem>
                    <span className='textTitle'>Fecha:</span>
                    <span> </span>
                    <span>{getDate(resCard.date)}</span>
                </ListGroupItem>
                <ListGroupItem>
                    <span className='textTitle'>Plomero:</span>
                    <span> </span>
                    <span>{resCard.inspeccion.name_plumber}</span>
                </ListGroupItem>
                <ListGroupItem>
                    <span className='textTitle'>Descripción:</span>
                    <span> </span>
                    <span>{resCard.description}</span>

                </ListGroupItem>
                <ListGroupItem>
                    <span className='textTitle'>Socio:</span>
                    <span> </span>
                    <span>{resCard.inspeccion.name_pay}</span>

                </ListGroupItem>
                <ListGroupItem>
                    <span className='textTitle'>Dirección:</span>
                    <span> </span>
                    <span>{resCard.reclamo.direction}</span>

                </ListGroupItem>
                <ListGroupItem>
                    <span className='textTitle'>Costo:</span>
                    <span> </span>
                    <span>{resCard.repair_cost+" Bs."}</span>

                </ListGroupItem>
                <Card.Body className='cardClaimBody'>
                    <Card.Title className='title'>Materiales</Card.Title>
                    <ListMaterials ress={resCard.lista_materiales} />
                </Card.Body>


            </ListGroup>

        </Card >



    )
}

export default CardWorkDone
