import React from 'react'
import '../../Styles/styleWorkDone.css'
import { Alert, Container, Button, Col, Card, FormGroup, Form, InputGroup, FormControl, Row } from 'react-bootstrap';
import { WorkDoneForm } from '../../Hook/Forms/WorkDoneForm';
import { useLocation } from 'react-router-dom';


function CreateWorkDone() {
    const location = useLocation();
    const Iid = location.state.proopInspection.inspections_id;
    const Cid = location.state.proopInspection.reclamo.claims_id;
    const name = location.state.proopInspection.name_plumber;
    const direction = location.state.proopInspection.reclamo.direction;

    const workDone = {
        inspections_id: Iid,
        claims_id: Cid,
        date: new Date(),
        description: '',
        repair_cost: '',
        stated: "PENDIENTE"
    };


    const {
        workDones, errors, handlerChange, handlerBlur, handlerSubmit,
    } = WorkDoneForm(workDone)

    return (
        <Container className='ContBodyCret'>
            <Row>
                <Col md={{ span: 10, offset: 1 }}>
                    <Card.Body>
                        <h3 className='title'>CREAR INFORME DE TRABAJO REALIZADO</h3>
                        <FormGroup className='bodyP'>

                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='dateC'>FECHA</InputGroup.Text>
                                <FormControl
                                    placeholder={new Date().toLocaleDateString()}
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    disabled
                                />
                            </InputGroup>

                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='Plumber'>PLOMERO</InputGroup.Text>
                                <FormControl
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    placeholder={name}
                                    disabled
                                />
                            </InputGroup>

                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='borderLeft-M'>DESCRIPCIÓN</InputGroup.Text>
                                <FormControl
                                    as='textarea'
                                    aria-label="With textarea"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='description'
                                    style={{ height: '150px' }}

                                    onChange={handlerChange}
                                    value={workDones.description}
                                />
                            </InputGroup>
                            {errors.description && <Alert variant={'danger'}>{errors.description} </Alert>}

                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='direct'>DIRECCION</InputGroup.Text>
                                <FormControl
                                    aria-label="Default select example"
                                    aria-describedby="inputGroup-sizing-default"
                                    placeholder={direction}
                                    disabled
                                >
                                </FormControl>
                            </InputGroup>


                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='cost'>COSTO DE REPARACION</InputGroup.Text>
                                <FormControl
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='repair_cost'
                                    value={workDones.repair_cost}
                                    onChange={handlerChange}

                                />
                                <InputGroup.Text>Bs.</InputGroup.Text>
                            </InputGroup>
                            {errors.repair_cost && <Alert variant={'danger'}>{errors.repair_cost} </Alert>}
                        </FormGroup>
                        <Form className='FormBody' onSubmit={handlerSubmit}>
                            <Button className='buttonCre' onClick={handlerBlur} variant='primary' type='submit'>
                                Crear
                            </Button>
                            <Button className='buttonCan' variant='danger' href='/ListInspections'>
                                Cancelar
                            </Button>
                        </Form>

                    </Card.Body>
                </Col>
            </Row>
        </Container>
    )
}

export default CreateWorkDone
