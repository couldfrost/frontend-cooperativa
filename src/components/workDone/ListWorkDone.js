import React from 'react'
import { Container, Row, Col,Alert} from 'react-bootstrap';
import { MDBTable, MDBTableHead, MDBTableBody } from 'mdb-react-ui-kit';
import TableWorkDone from './TableWorkDone';
import { GetWorkDone } from '../../Hook/Gets/GetWorkDone';
import { useLocation } from 'react-router-dom';
function ListWorkDone() {
    const location = useLocation();
    const {workDones} = GetWorkDone( );
    
  return (
    <Container className='ContBody'>
            <Row>
                <Col>
                {location.state? <Alert variant='success' >{location.state.response}</Alert>:<></>}
                    <h3 className='title'>MOSTRAR INFORMES DE TRABAJOS REALIZADOS</h3>
                    <MDBTable responsive>
                        <MDBTableHead className='table-info'>
                            <tr>
                                <th scope='col'>FECHA</th>
                                <th scope='col'>ESTADO</th>
                                <th></th>
                            </tr>
                        </MDBTableHead>
                        <MDBTableBody>

                            {

                               
                              workDones.map(workDone=><TableWorkDone key={workDone.work_done_id} ResWorkDone={workDone}/> )
                              
                            }

                        </MDBTableBody>
                    </MDBTable>
                </Col>
            </Row>
        </Container>
  )
}

export default ListWorkDone
