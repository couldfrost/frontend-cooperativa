import React from 'react'
import '../../Styles/styleTabla.css'
import ShowWorkDone from './ShowWorkDone'
import { color } from '../../function/CssAndViewShow'

function TableWorkDone({ResWorkDone}) {
   
    return (
        <tr style={color(ResWorkDone.stated)}>
            <td className='word'>{ResWorkDone.date}</td>
            <td className='word'>{ResWorkDone.stated}</td>
            <td><ShowWorkDone respon={ResWorkDone}/></td>    


        </tr>
    )
}

export default TableWorkDone
