import React from 'react'
import { Button } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom';
import UpdateState from './UpdateState';
function TablePartner({ resPartner }) {

    const navigate = useNavigate();
    const onSubmit = () => {

        navigate('/InsertMeter', { state: resPartner.partner_id })


    }

    return (
        <tr>
            <td>{resPartner.ci}</td>
            <td>{resPartner.partner_name}</td>
            <td>{resPartner.surname_father}</td>
            <td>{resPartner.surname_mother}</td>
            <td>{resPartner.date_birth}</td>
            <td>{resPartner.direction}</td>
            <td>{resPartner.phone}</td>
            <td>{resPartner.stated}</td>
            <td>{resPartner.medidores.map((medidor) => medidor.number_meter + ",")}</td>
            {
                <td>

                    <Button variant='warning' type='Submit' onClick={() => onSubmit(resPartner.partner_id)}>Añadir</Button>

                </td>
            }
            {
                <td>

                    <UpdateState id={resPartner.partner_id}/>

                </td>

            }
        </tr>
    )
}

export default TablePartner
