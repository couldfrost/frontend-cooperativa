import React from 'react'
import { Container, Row, Col, Card, FormGroup, InputGroup, FormControl, Button, Alert } from 'react-bootstrap';
import { MDBTable, MDBTableHead, MDBTableBody } from 'mdb-react-ui-kit';
import TablePartner from './TablePartner';
import { useLocation } from 'react-router-dom';
import { SearchPartners } from '../../Hook/Searchs/SearchPartners';
function ListPartners() {
    const location = useLocation();
    const {ci,partners,onCiChange,onSubmit}=SearchPartners();
    
    
    return (
        <Container className='ContBody'>
            <Row>
                <Col>
                {location.state? <Alert variant='success' >{location.state.response}</Alert>:<></>}
                    <h3 className='title'>MOSTRAR SOCIOS</h3>
                    <Card.Body>
                        <FormGroup>
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default">CI</InputGroup.Text>
                                <FormControl
                                    name='ci'
                                    value={ci}
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    onChange={onCiChange}

                                />
                                <Button variant='primary' type='submit' onClick={onSubmit}>
                                    Confirmar
                                </Button>
                            </InputGroup>
                        </FormGroup>
                    </Card.Body>
                    <MDBTable responsive>
                        <MDBTableHead className='table-info'>
                            <tr>
                                <th scope='col'>CI</th>
                                <th scope='col'>NOMBRE</th>
                                <th scope='col'>AP PATERNO</th>
                                <th scope='col'>AP MATERNO</th>
                                <th scope='col'>FEC NACIMIENTO</th>
                                <th scope='col'>DIRECCIÓN</th>
                                <th scope='col'>TELÉFONO</th>
                                <th scope='col'>ESTADO</th>
                                <th scope='col'>MEDIDOR</th>
                                <th scope='col'>INSERTAR MEDIDOR</th>
                                <th scope='col'>CAMBIO ESTADO</th>
                                
                                
                            </tr>
                        </MDBTableHead>
                        <MDBTableBody>

                            {

                                partners.map(partner => <TablePartner key={partner.partner_id} resPartner={partner} />)

                            }

                        </MDBTableBody>
                    </MDBTable>
                </Col>
            </Row>
        </Container>
    )
}

export default ListPartners
