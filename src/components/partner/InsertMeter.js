import React from 'react'
import { Alert, Container, Button, Col, Card, FormGroup, Form, InputGroup, FormControl, Row } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';
import '../../Styles/styleMeter.css'
import { MeterForm } from '../../Hook/Forms/MeterForm'
function InsertMeter() {
    const Location = useLocation();
    const Pid = Location.state;
    const meter = {
        partner_id: Pid,
        number_meter: '',
        direction_meter: '',
        date_registered: ''
    };
    const {
        meters, errors, handlerChange, handlerBlur, handlerSubmit
    } = MeterForm(meter);
    return (
        <Container className='ContBodyCret'>
            <Row>
                <Col md={{ span: 10, offset: 1 }}>
                    <Card.Body>
                        {errors.meter && <Alert variant={'danger'}>{errors.meter} </Alert>}
                        <h3 className='title'>INSERTAR MEDIDOR</h3>
                        <FormGroup className='bodyP'>
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='nmeter'>N° Medidor</InputGroup.Text>
                                <FormControl
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"

                                    name='number_meter'
                                    value={meters.number_meter}
                                    onChange={handlerChange}


                                />
                            </InputGroup>
                            {errors.number_meter && <Alert variant={'danger'}>{errors.number_meter} </Alert>}
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='dmeter'>Dirección Medidor</InputGroup.Text>
                                <FormControl
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"

                                    name='direction_meter'
                                    value={meters.direction_meter}
                                    onChange={handlerChange}


                                />
                            </InputGroup>
                            {errors.direction_meter && <Alert variant={'danger'}>{errors.direction_meter} </Alert>}
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='date'>FECHA REGISTRO</InputGroup.Text>
                                <FormControl
                                    type='date'
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='date_registered'
                                    value={meters.date_registered}
                                    onChange={handlerChange}
                                />
                            </InputGroup>
                        </FormGroup>
                        <Form className='FormBody' onSubmit={handlerSubmit} >
                            <Button className='buttonCre' onClick={handlerBlur} variant='primary' type='submit'>
                                Insertar
                            </Button>
                            <Button className='buttonCan' variant='danger' href='/ListPartners'>
                                Cancelar
                            </Button>
                        </Form>

                    </Card.Body>
                </Col>
            </Row>
        </Container>
    )
}

export default InsertMeter
