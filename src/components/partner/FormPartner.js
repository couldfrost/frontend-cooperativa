import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../../Styles/stylePartner.css';
import { Alert, Container, Button, Col, Card, FormGroup, Form, InputGroup, FormControl, Row } from 'react-bootstrap';
function FormPartner({ resPartner }) {
    return (
        <Container className='ContBodyCret'>
            <Row>
                <Col md={{ span: 10, offset: 1 }}>
                    <Card.Body>
                        {resPartner.errors.partner && <Alert variant={'danger'}>{resPartner.errors.partner} </Alert>}
                        <h3 className='title'>CREAR NUEVO SOCIO</h3>

                        <FormGroup className='bodyP'>
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='ci'>CI</InputGroup.Text>
                                <FormControl
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='ci'
                                    value={resPartner.partners.ci}
                                    onChange={resPartner.handlerChange}

                                />
                            </InputGroup>
                            {resPartner.errors.ci && <Alert variant={'danger'}>{resPartner.errors.ci} </Alert>}
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='name'>NOMBRE</InputGroup.Text>
                                <FormControl
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='partner_name'
                                    value={resPartner.partners.partner_name}
                                    onChange={resPartner.handlerChange}

                                />
                            </InputGroup>
                            {resPartner.errors.partner_name && <Alert variant={'danger'}>{resPartner.errors.partner_name} </Alert>}
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='nameP'>APELLIDO PATERNO</InputGroup.Text>
                                <FormControl
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='surname_father'
                                    value={resPartner.partners.surname_father}
                                    onChange={resPartner.handlerChange}

                                />
                            </InputGroup>
                            {resPartner.errors.surname_father && <Alert variant={'danger'}>{resPartner.errors.surname_father} </Alert>}
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='nameM'>APELLIDO MATERNO</InputGroup.Text>
                                <FormControl
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='surname_mother'
                                    value={resPartner.partners.surname_mother}
                                    onChange={resPartner.handlerChange}

                                />
                            </InputGroup>
                            {resPartner.errors.surname_mother && <Alert variant={'danger'}>{resPartner.errors.surname_mother} </Alert>}
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='borderLeft-D'>FECHA NACIMIENTO</InputGroup.Text>
                                <FormControl
                                    type='date'
                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='date_birth'
                                    value={resPartner.partners.date_birth}
                                    onChange={resPartner.handlerChange}

                                />
                            </InputGroup>
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='direction'>DIRECCIÓN</InputGroup.Text>
                                <FormControl

                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='direction'
                                    value={resPartner.partners.direction}
                                    onChange={resPartner.handlerChange}

                                />
                            </InputGroup>
                            {resPartner.errors.direction && <Alert variant={'danger'}>{resPartner.errors.direction} </Alert>}
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='phone'>TELÉFONO</InputGroup.Text>
                                <FormControl

                                    aria-label="DefaultPar"
                                    aria-describedby="inputGroup-sizing-default"
                                    name='phone'
                                    value={resPartner.partners.phone}
                                    onChange={resPartner.handlerChange}

                                />
                            </InputGroup>
                            {resPartner.errors.phone && <Alert variant={'danger'}>{resPartner.errors.phone} </Alert>}
                            
                            <InputGroup className="mb-3">
                                <InputGroup.Text id="inputGroup-sizing-default" className='name'>ESTADO</InputGroup.Text>
                                <Form.Select

                                    name='stated'
                                    value={resPartner.partners.stated}
                                    onChange={resPartner.handlerChange}
                                >
                                    <option>Selecciona</option>   
                                    <option value="ACTIVO">ACTIVO</option>
                                    <option value="PASIVO">PASIVO</option>
                                    <option value="ELIMINADO">ELIMINADO</option>

                                </Form.Select>
                            </InputGroup>
                        </FormGroup>
                        <Form className='FormBody' onSubmit={resPartner.handlerSubmit}  >

                            <Button className='buttonCre' onClick={resPartner.handlerBlur} variant='primary' type='submit'>
                                Crear
                            </Button>

                            <Button className='buttonCan' variant='danger' href='/' >
                                Cancelar
                            </Button>
                        </Form>

                    </Card.Body>
                </Col>
            </Row>
        </Container>
    )
}

export default FormPartner
