import React from 'react'
import { Modal, Button, InputGroup, Form } from 'react-bootstrap'
import {UpdateStateForm} from '../../Hook/Forms/UpdateStateForm'
function UpdateState({id}) {
    const updates={
        stated:' '
    }
    const {update, show, handlerChange, handlerSubmit, handleClose, handleShow}=UpdateStateForm(updates,id)
  return (
    <>
        <Button variant='success' onClick={handleShow}>Cambiar</Button>
                                
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Cambiar Estado</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <InputGroup className="mb-3">
                    <InputGroup.Text id="inputGroup-sizing-default" className='borderLeft-D'>ESTADO</InputGroup.Text>
                    <Form.Select

                                    name='stated'
                                    value={update.stated}
                                    onChange={handlerChange}
                                >
                                    <option>Selecciona</option>   
                                    <option value="ACTIVO">ACTIVO</option>
                                    <option value="PASIVO">PASIVO</option>
                                    <option value="ELIMINADO">ELIMINADO</option>

                                </Form.Select>
                </InputGroup>
               
            </Modal.Body>
            <Modal.Footer>
                
                    <Button className='buttonConf' onClick={handlerSubmit} >Cambiar</Button>
                 
            </Modal.Footer>
        </Modal>
        </>
  )
}

export default UpdateState
