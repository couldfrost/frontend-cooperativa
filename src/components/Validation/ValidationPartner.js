
const validationForm = (variables) => {
    let errors = {};
    let regexName = /^[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]+$/;
    
    let regexComments = /^.{1,65}$/;
    let regexNumber = /^[0-9]+$/;
    if (!variables.ci.trim()) {
        errors.ci = "Este campo es requerido";
    }
    else{
        if(!regexNumber.test(variables.ci.trim()))
        {
           errors.ci ="Este campo solo acepta numeros";
        }
        
    };
    if (!variables.partner_name.trim()) {
        errors.partner_name = "Este campo es requerido";
    }
    else{
        if(!regexName.test(variables.partner_name.trim())){
            errors.partner_name ="Este campo solo acepta letras";
        }
    };
    if (!variables.surname_father.trim()) {
        errors.surname_father = "Este campo es requerido";
    }
    else{
        if(!regexName.test(variables.surname_father.trim())){
            errors.surname_father = "Este campo solo acepta letras";
        }
    };
    if (!variables.surname_mother.trim()) {
        errors.surname_mother = "Este campo es requerido";
    }
    else{
        if(!regexName.test(variables.surname_mother.trim())){
            errors.surname_mother = "Este campo solo acepta letras";
        }
    };
    if (!variables.direction.trim()) {
        errors.direction = "Este campo es requerido";
    }
    else{
        if(!regexComments.test(variables.direction.trim()))
        {
            errors.direction ="Este campo es máximo de 65 caracteres";
        }
    };
    if (!variables.phone.trim()) {
        errors.phone = "Este campo es requerido";
    }
    else{
        if(!regexNumber.test(variables.phone.trim()))
        {
           errors.phone ="Este campo solo acepta numeros";
        }
        
    };
    return errors;

};

export { validationForm };