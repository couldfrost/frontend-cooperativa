const validationForm = (workDones, update) => {
    let errors = {};
    
    let regexComments = /^.{1,300}$/;
    let regexNumber = /^[0-9/./]+$/;
    
    if (update === "REGISTER") {
        if (!workDones.description.trim()) {
            errors.description = "La descripción es requerido";
        }
        else {
            if (!regexComments.test(workDones.description.trim())) {
                errors.description = "Máximo de caracteres 300";
            }
        }
    }
    if (!workDones.repair_cost.trim()) {

        errors.repair_cost = "El costo de la reparación es requerido"
    }
    else {
        if (!regexNumber.test(workDones.repair_cost.trim())) {
            errors.repair_cost = "Este campo solo acepta numeros"
        }
    }
    return errors;
};
export { validationForm };