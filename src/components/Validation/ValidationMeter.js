const validationForm = (variables) => {
    let errors = {};
    
    let regexComments = /^.{1,65}$/;
    let regexNumber = /^[0-9]+$/;
    if (!variables.number_meter.trim()) {
        errors.number_meter = "Este campo es requerido"
    }
    else {
        if (!regexNumber.test(variables.number_meter.trim())) {
            errors.number_meter = "Este campo solo acepta numeros"
        }
    }

    if (!variables.direction_meter.trim()) {
        errors.direction_meter = "Este campo es requerido"
    }
    else{
        if(!regexComments.test(variables.direction_meter.trim())){
            errors.direction_meter = "Máximo de tamaño 65 caracteres"
        }
    }


    return errors;

};

export { validationForm };