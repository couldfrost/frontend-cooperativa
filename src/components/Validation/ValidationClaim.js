const validationForm = (claims) => {
    let errors = {};
    
    let regexDirection = /^.{1,65}$/;
    let regexDescripcion = /^.{1,350}$/;
    let regexNumber = /^[0-9]+$/;
    if (!claims.phone.trim()) {
        errors.phone = "Teléfono es requerido"
    } else {
        if (!regexNumber.test(claims.phone.trim())) {
            errors.phone = "Este campo solo acepta numeros"
        }
    }
    if (!claims.direction.trim()) {
        errors.direction = "La dirección es requerido"
    }else{
        if(!regexDirection.test(claims.direction.trim())){
            errors.direction = "Máximo de caracteres aceptado 65"
        }
    }
    if (!claims.descriptions.trim()) {
        errors.descriptions = "La descripción es requerido"
    }else{
        if(!regexDescripcion.test(claims.descriptions.trim())){
            errors.descriptions = "Máximo de caracteres aceptado 350"
        }
    }
    return errors;
};
export {validationForm}