const validationForm = (authentic) => {
    let errors = {};
  
    let regexNumber = /^[0-9]+$/;
    if (!authentic.ci.trim()) {
        errors.ci = "Este campo esta vacío"
    } else {
        if (!regexNumber.test(authentic.ci.trim())) {
            errors.ci = "Este campo solo acepta numeros"
        }
    }
    return errors;
};
export {validationForm}