const validationForm = (variables,update) => {
    let errors = {};
    let regexName = /^[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]+$/;
   
    if(update==="UPDATE"){
    
    if (!variables.name.trim()) {
        errors.name = "Este campo es requerido"
    }
    else{
        if(!regexName.test(variables.name.trim())){
            errors.name= "Este campo solo acepta letras"
        }
    }
 
    }
    
    if (!variables.password.trim()) {
        errors.password = "Este campo es requerido"
    }
    else{
        if(variables.password.length<=6){
            errors.password="El campo tiene que tener 6 o mas caracteres"
        }
    }
    if (!variables.resPassword.trim()) {
        errors.resPassword = "Este campo es requerido"
    }
    else{
        if(variables.resPassword!==variables.password){
            errors.resPassword="No es el mismo password"
        }
    }

    return errors;

};

export { validationForm };