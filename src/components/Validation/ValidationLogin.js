const validationForm = (variables) => {
    let errors = {};
    
    if (!variables.email.trim()) {
        errors.email = "Este campo es requerido"
    }
    if (!variables.password.trim()) {
        errors.password = "Este campo es requerido"
    }
  
    
    return errors;

};

export { validationForm };