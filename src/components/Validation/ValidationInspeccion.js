const validationForm = (inspections) => {
    let errors = {};
    let regexName = /^[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]+$/;
    
    let regexDescripcion = /^.{1,300}$/;
   
    if (!inspections.name_plumber.trim()) {
        errors.name_plumber = "El nombre del plomero es requerido"
    } else {
        if (!regexName.test(inspections.name_plumber.trim())) {
            errors.name_plumber = "Este campo solo acepta letras"
        }
    }
    if (!inspections.description.trim()) {
        errors.description = "La descripción es requerido"
    }
    else{
        if(!regexDescripcion.test(inspections.description.trim()))
        {
           errors.description="cantidad máxima de caracteres 300"
        }
    }
   
    return errors;
};
export {validationForm}