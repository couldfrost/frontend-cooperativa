const validationForm = (addMaterials) => {
    let errors = {};
    
    let regexComments = /^.{1,100}$/;
    let regexNumber = /^[0-9/./]+$/;
    
    if (!addMaterials.amount.trim()) {
        errors.amount = "Cantidad es requerida"
    }
    else{
        if(!regexNumber.test(addMaterials.amount.trim())){
             errors.amount = "Este campo solo acepta numeros"
        }
    }

        if(!regexComments.test(addMaterials.observation)){
            errors.observation="Máximo de tamaño 100"
        }
    
    
    return errors;
};
export {validationForm}