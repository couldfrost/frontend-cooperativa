const validationForm = (materials) => {
    let errors = {};
  
  
    if (!materials.material_name.trim()) {
        errors.material_name = "Este campo no debe estar vacío"
    }
  
    return errors;
};
export {validationForm}