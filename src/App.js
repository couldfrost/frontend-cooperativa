import './App.css';
import Home from './components/home';
import Navegation from './components/Permanent/Navegation';
import ListClaims from './components/claim/ListClaims';
import ClaimsPartner from './components/claim/ClaimsPartner';
import CreateClaim from './components/claim/CreateClaim';
import Header from './components/Permanent/Header';
import Footer from './components/Permanent/Footer';
import AuthenticPartner from './components/claim/AuthenticPartner';
import AuthenticShowPartner from './components/claim/AuthenticShowPartner';
import CreatePartner from './components/partner/CreatePartner';
import CreateInspection from './components/inspection/CreateInspection';
import ListInspections from './components/inspection/ListInspections';
import ListPartners from './components/partner/ListPartners';
import RegisterUser from './components/user/RegisterUser';
import ListUsers from './components/user/ListUsers';
import Login from './components/user/Login';
import InsertMeter from './components/partner/InsertMeter';
import ForgotPassword from './components/user/ForgotPassword';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import CreateWorkDone from './components/workDone/CreateWorkDone';
import CreateListMaterials from './components/ListMaterials/CreateListMaterials';
import ListWorkDone from './components/workDone/ListWorkDone';
import PrivateRoute from './components/PrivateRoute';
import MapeoClaims from './components/Maps/MapeoClaims';
import ListHistory from './components/History.js/ListHistory';
import { UserProvider } from './components/UserContext';

function App() {

  return (

    <Router>
      <Header />
      <UserProvider>
        <Navegation />
        
        <div >
        
          <Routes>
            <Route path='/' exact element={<Home />} />
            
            <Route path='/CreatePartner' element={<PrivateRoute><CreatePartner/></PrivateRoute>} />
            <Route path='/ListPartners' element={<PrivateRoute><ListPartners /></PrivateRoute>} />
            <Route path='/ListClaims' element={<ListClaims />} />
            <Route path='/CreateClaim' element={<CreateClaim />} />
            <Route path='/AuthenticPartner' element={<AuthenticPartner />} />
            <Route path='/AuthenticShowPartner' element={<AuthenticShowPartner />} />
            <Route path='/ClaimsPartner' element={<ClaimsPartner/>} />
            <Route path='/RegisterUser' element={<RegisterUser />} />
            <Route path='/ListUsers' element={<PrivateRoute><ListUsers /></PrivateRoute>} />
            <Route path='/Login' element={<Login />} />
            <Route path='/InsertMeter' element={<PrivateRoute><InsertMeter/></PrivateRoute>}/>
            <Route path='/CreateInspection' element={<PrivateRoute><CreateInspection/></PrivateRoute>}/>
            <Route path='/ListInspections' element={<PrivateRoute><ListInspections/></PrivateRoute>}/>
            <Route path='/CreateWorkDone' element={<PrivateRoute><CreateWorkDone/></PrivateRoute>}/>
            <Route path='/CreateListMaterial' element={<PrivateRoute><CreateListMaterials/></PrivateRoute>}/>
            <Route path='/ForgotPassword' element={<ForgotPassword/>}/>
            <Route path='/ListWorkDone' element={<PrivateRoute><ListWorkDone/></PrivateRoute>}/>
            <Route path='/ListHistory' element={<ListHistory/>}/>
            <Route path='/Map' element={<MapeoClaims/>}/>
            
          </Routes>
        
        
        </div>
      </UserProvider>
      <Footer />
    </Router>


  );
}

export default App;
