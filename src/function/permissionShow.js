
const permissionAdmin=(sesion)=>{
    let res=false;
    if(sesion){
        if(sesion.rol==="ADMIN" || sesion.rol==="DIRECTIVA"){
            res=true
        }
        else{ res=false;}
       
    }
    return res;
}
const permissionDirective=(sesion,list,stated)=>{
    let res=false;
    if(sesion){
        if(sesion.rol==="DIRECTIVA"||sesion.rol==="ADMIN"){
            if (list) {
                res = false;
            }
            else {
                if(stated)
                {
                    if(stated==="PENDIENTE")
                    {
                    res= true;
                    }
                    else{
                        res=false;
                    }
                }
                else{
                res = true;
                }
            }
        }
        else{ res=false;}
       
    }
    return res;
}

const permissionSecretary=(sesion,list,stated)=>{
    let res=false;
    if(sesion){
        if(sesion.rol==="SECRETARIA"||sesion.rol==="ADMIN"){
            if (list) {
                res = false;
            }
            else {
                
                if(stated)
                {
                    if(stated==="ACEPTADO"||stated==="CONFIRMADO")
                    {
                    res= true;
                    }
                    else{
                        res=false;
                    }
                }
                else{
                res = true;
                }
            }
        }
        else{ res=false;}
       
    }
    return res;
}


const permissionPlumber=(sesion,list,stated)=>{
    let res=false;
    if(sesion){
        if(sesion.rol==="PLOMERO"||sesion.rol==="ADMIN"){
            if (list) {
                res = false;
            }
            else {
                
                if(stated)
                {
                    if(stated==="ACEPTADO")
                    {
                    res= true;
                    }
                    else{
                        res=false;
                    }
                }
                else{
                res = true;
                }
            }
        }
        else{ res=false;}
       
    }
    return res;
}
const permissionPlumber2=(sesion,list,stated)=>{
    let res=false;
    if(sesion){
        if(sesion.rol==="PLOMERO"||sesion.rol==="ADMIN"){
            if (list) {
                res = false;
            }
            else {
                
                if(stated)
                {
                    if(stated==="PENDIENTE")
                    {
                    res= true;
                    }
                    else{
                        res=false;
                    }
                }
                else{
                res = true;
                }
            }
        }
        else{ res=false;}
       
    }
    return res;
}


export { permissionAdmin,permissionDirective,permissionPlumber,permissionPlumber2,permissionSecretary }