const getDate = (date) => {
    const values = date.split('-');
    return values[2] + '/' + values[1] + '/' + values[0];
}
const color = (stated) => {
    if (stated === "ACEPTADO" || stated === "CONFIRMADO") {
        return { backgroundColor: '#548F6F' }
    }
    if (stated === "PENDIENTE") {
        return { backgroundColor: '#EBA41E' }
    }
    if (stated === "CANCELADO") {
        return { backgroundColor: 'red' }
    }

}
const showComment = (comment) => {
    if (comment === null || comment === " ") {
        return false;
    }
    else {
        return true;
    }
}
export { showComment, color, getDate }